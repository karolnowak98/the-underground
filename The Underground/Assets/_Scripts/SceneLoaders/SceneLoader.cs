using System;
using UnityEngine.SceneManagement;

namespace Underground.SceneLoaders
{
    public class SceneLoader
    {
        public void LoadMainMenuScene()
        {
            SceneManager.LoadScene(Keys.MainMenuSceneName);
        }

        public void LoadSelectLevelScene()
        {
            SceneManager.LoadScene(Keys.SelectLevelSceneName);
        }

        public void LoadGameOverMenuScene()
        {
            SceneManager.LoadScene(Keys.GameOverMenuSceneName);
        }

        public void LoadSceneByName(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        public void LoadCreditsMenuScene()
        {
            SceneManager.LoadScene(Keys.CreditsMenuSceneName);
        }

        public void LoadControlsMenuScene()
        {
            SceneManager.LoadScene(Keys.ControlsMenuSceneName);
        }

        internal void LoadWinMenuScene()
        {
            SceneManager.LoadScene(Keys.WinMenuSceneName);
        }
    }
}
