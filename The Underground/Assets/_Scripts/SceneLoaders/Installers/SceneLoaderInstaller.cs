using Zenject;

namespace Underground.SceneLoaders.Installers
{
    public class SceneLoaderInstaller : MonoInstaller<SceneLoaderInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<SceneLoader>().AsSingle();
        }
    }
}