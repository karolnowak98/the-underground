using UnityEngine;

namespace Underground.InteractableObjects
{
    public interface IInteractable
    {
        void Interact(GameObject interactor);
    }
}
