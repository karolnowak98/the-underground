using System.Collections;
using System.Collections.Generic;
using Underground.Audio;
using Underground.InteractableObjects;
using Underground.Maps.Settings;
using Underground.SceneLoaders;
using UnityEngine;
using Zenject;

namespace Underground.InteractableObjects
{
    [RequireComponent(typeof(Animator))]
    public class EndDoors : MonoBehaviour, IInteractable
    {
        private SceneLoader sceneLoader;
        private Animator animator;
        private AudioManager audioManager;
        private MapSettings mapSettings;
        [SerializeField] private int levelIdToUnlock;
        [SerializeField] private string sceneNameToGo;

        [Inject]
        private void Construct(SceneLoader sceneLoader, MapSettings mapSettings, Animator animator, AudioManager audioManager)
        {
            this.sceneLoader = sceneLoader;
            this.mapSettings = mapSettings;
            this.animator = animator;
            this.audioManager = audioManager;
        }

        public void Interact(GameObject interactor)
        {
            StartCoroutine(OpenDoor());
        }

        public IEnumerator OpenDoor()
        {
            PlayerPrefs.SetInt("Level" + levelIdToUnlock + "Completed", 1);
            mapSettings.LevelCompleted = true;
            animator.SetBool("opened", true);
            audioManager.Play("Open");
            yield return new WaitForSeconds(1.5f);
            sceneLoader.LoadSceneByName(sceneNameToGo);
        }
    }
}