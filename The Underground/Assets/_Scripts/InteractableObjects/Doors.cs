using System.Collections;
using Underground.Audio;
using UnityEngine;
using Zenject;

namespace Underground.InteractableObjects
{
    public class Doors : MonoBehaviour, IInteractable
    {
        [SerializeField] private Doors exitPoint;
        [SerializeField] private Animator anim;
        [SerializeField] private bool isEntrance;

        private AudioManager audioManager;

        [Inject]
        private void Construct(AudioManager audioManager)
        {
            this.audioManager = audioManager;
        }

        public void Interact(GameObject interactor)
        {
            TeleportToExitPoint(interactor);
        }

        private void TeleportToExitPoint(GameObject interactor)
        {
            interactor.transform.position = exitPoint.transform.position;
            StartCoroutine("PlayOpenDoorsAnimation");
            if (isEntrance)
            {
                audioManager.Play("Open");
            }
            else
            {
                audioManager.Play("Close");
            }
            exitPoint.StartCoroutine("PlayOpenDoorsAnimation");
        }

        public IEnumerator PlayOpenDoorsAnimation()
        {
            anim.SetBool("OpenDoors", true);
            yield return new WaitForSeconds(0.25f);
            anim.SetBool("OpenDoors", false);
        }
    }
}
