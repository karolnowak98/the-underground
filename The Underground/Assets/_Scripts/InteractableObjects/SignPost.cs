using System.Collections;
using System.Collections.Generic;
using TMPro;
using Underground.Audio;
using Underground.UI.InGame.Coins;
using UnityEngine;
using Zenject;

namespace Underground.InteractableObjects
{
    public class SignPost : MonoBehaviour, IInteractable
    {
        [SerializeField] GameObject item;
        [SerializeField] int requiredGold;
        [SerializeField] TMP_Text cost;

        private PlayerCoins playerCoins;
        private bool isBought = false;
        private AudioManager audioManager;

        [Inject]
        private void Construct(PlayerCoins playerCoins, AudioManager audioManager)
        {
            this.playerCoins = playerCoins;
            this.audioManager = audioManager;
        }

        private void Start()
        {
            cost.SetText($"{requiredGold}");
        }

        public void Interact(GameObject interactor)
        {
            if(item != null)
            {
                if(playerCoins.Coins >= requiredGold)
                {
                    item.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    if (!isBought)
                    {
                        audioManager.Play("Pay");
                        playerCoins.Coins -= requiredGold;
                    }
                    isBought = true;
                }
            }
        }
    }
}