namespace Underground.InteractableObjects
{
    public interface IInteractor
    {
        IInteractable ClosestInteractable { get; set; }
    }
}
