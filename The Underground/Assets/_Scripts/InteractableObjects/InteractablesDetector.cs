using UnityEngine;

namespace Underground.InteractableObjects
{
    public class InteractablesDetector : MonoBehaviour
    {
        private IInteractor interactor;

        private void Awake()
        {
            interactor = GetComponentInParent<IInteractor>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<IInteractable>() != null)
            {
                interactor.ClosestInteractable = collision.gameObject.GetComponent<IInteractable>();
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<IInteractable>() != null && interactor.ClosestInteractable == collision.gameObject.GetComponent<IInteractable>())
            {
                interactor.ClosestInteractable = null;
            }
        }
    }
}
