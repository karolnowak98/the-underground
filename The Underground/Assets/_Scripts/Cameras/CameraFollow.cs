using UnityEngine;

namespace Underground.Cameras
{
    public class CameraFollow
    {
        private Camera camera;
        private Transform target;
        private Vector3 offset;
        private float smoothing;

        public CameraFollow(
            Camera camera,
            Transform target)
        {
            this.camera = camera;
            this.target = target;
        }

        public void Init()
        {
            smoothing = 5f;
            camera.transform.position = target.position + Vector3.back * 10f;
            offset = camera.transform.position - target.position;
        }

        public void SetTarget(Transform target)
        {
            camera.transform.position = target.position + Vector3.back * 10f;
            this.target = target;
            offset = camera.transform.position - target.position;
        }

        public void FixedTick()
        {
            Vector3 targetCamPos = target.position + offset;
            camera.transform.position = Vector3.Lerp(camera.transform.position, targetCamPos, smoothing * Time.fixedDeltaTime);
        }
    }
}

