using UnityEngine;
using Zenject;

namespace Underground.Cameras.Installers
{
    public class CameraFollowInstaller : MonoInstaller<CameraFollowInstaller>
    {
        [SerializeField] private Transform player; 
        [SerializeField] private Camera sceneCamera; 

        public override void InstallBindings()
        {
            Container.Bind<CameraFollow>().FromSubContainerResolve().ByMethod(InstallCameraFollow).AsSingle();
        }

        private void InstallCameraFollow(DiContainer subContainer)
        {
            subContainer.Bind<CameraFollow>().AsSingle();

            subContainer.BindInstance(player).AsSingle();
            subContainer.BindInstance(sceneCamera).AsSingle();
        }
    }
}
