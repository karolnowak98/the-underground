namespace Underground
{
    public static class Keys
    {
        public static string MainMenuSceneName = "MainMenu";
        public static string OptionsSceneName = "Options";
        public static string SelectLevelSceneName = "SelectLevelMenu";
        public static string GameDemoSceneName = "GameDemo";
        public static string GameOverMenuSceneName = "GameOverMenu";
        public static string CreditsMenuSceneName = "CreditsMenu";
        public static string ControlsMenuSceneName = "ControlsMenu";
        public static string WinMenuSceneName = "WinMenu";
    }
}