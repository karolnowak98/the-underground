using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using UnityEngine.InputSystem;
using Underground.Players.States.Factories;
using Underground.Items.Traps.Signals;
using Underground.Audio;
using Underground.Items.Signals;
using Underground.Maps;
using Underground.Audio.SoundMaps;

namespace Underground.Players.States
{
    public class RunPlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private SignalBus _signalBus;
        private Rigidbody2D _rigidbody2D;
        private IdlePlayerStateFactory _idlePlayerStateFactory;
        private SneakPlayerStateFactory _sneakPlayerStateFactory;
        private ClimbPlayerStateFactory _climbPlayerStateFactory;
        private FallPlayerStateFactory _fallPlayerStateFactory;
        private AudioManager _audioManager;
        private SoundMapAgent _soundMapAgent;
        public RunPlayerState(Animator animator,
            PlayerSettings playerSettings,
            Rigidbody2D rigidbody2D,
            Player player,
            SignalBus signalBus,
            AudioManager audioManager,
            SoundMapAgent soundMapAgent,
            IdlePlayerStateFactory idlePlayerStateFactory,
            SneakPlayerStateFactory sneakPlayerStateFactory,
            ClimbPlayerStateFactory climbPlayerStateFactory,
            FallPlayerStateFactory fallPlayerStateFactory
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _rigidbody2D = rigidbody2D;
            _player = player;
            _signalBus = signalBus;
            _audioManager = audioManager;
            _soundMapAgent = soundMapAgent;
            _idlePlayerStateFactory = idlePlayerStateFactory;
            _sneakPlayerStateFactory = sneakPlayerStateFactory;
            _climbPlayerStateFactory = climbPlayerStateFactory;
            _fallPlayerStateFactory = fallPlayerStateFactory;
        }

        public void Deinit()
        {
            _audioManager.StopPlaying();
        }

        public void Init()
        {
            Debug.Log("entered run state");
            _animator.SetFloat("Speed", 1f);
        }

        public void Tick()
        {
            if (_rigidbody2D.gravityScale == 0)
                _rigidbody2D.gravityScale = _playerSettings.DefaultGravityScale;
             
            if (_player.Movement.x == 0)
                _player.SetState(_idlePlayerStateFactory.CreateState());
            else if (_player.IsLadderAbove() && _player.IsLadderBelow())
                _player.SetState(_climbPlayerStateFactory.CreateState());
            else if (_player.IsLadderAbove() || _player.IsLadderBelow() )
            {
                if(Mathf.Abs(_player.Movement.y) > 0.01f)
                    _player.SetState(_climbPlayerStateFactory.CreateState());
                    
                _rigidbody2D.gravityScale = 0f;
            }
            else if (!_player.IsPlayerGrounded() && !_player.IsLadderBelow())
                _player.SetState(_fallPlayerStateFactory.CreateState());
            else
            {
                _audioManager.Play("step");
                _soundMapAgent.AddNoise(_player.transform.position, _playerSettings.RunLoudness);
            }
                
        }
        public void FixedTick()
        {
            Move();
        }

        private void Move()
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + new Vector2(_player.Movement.x * _playerSettings.RunSpeed * Time.fixedDeltaTime, 0)); 
        }


        public void UseItem(InputAction.CallbackContext context)
        {
            if(context.performed)
                _signalBus.Fire<UseItemSignal>();
        }

        public void Stealth(InputAction.CallbackContext context)
        {
            if (Keyboard.current.leftShiftKey.wasPressedThisFrame)
                _player.SetState(_sneakPlayerStateFactory.CreateState());
        }
        public void ChooseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<ChooseItemSignal>();
        }

        public class Factory : PlaceholderFactory<RunPlayerState>
        {}
    }
}
