using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using Underground.Players.States.Factories;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using Underground.Items.Traps.Signals;
using Underground.Items.Signals;
using Underground.Audio;
using System;

namespace Underground.Players.States
{
    public class IdlePlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private SignalBus _signalBus;
        private RunPlayerStateFactory _runPlayerStateFactory;
        private SneakPlayerStateFactory _sneakPlayerStateFactory;
        private ClimbPlayerStateFactory _climbPlayerStateFactory;
        private Rigidbody2D _rigidbody2D;
        private AudioManager _audioManager;
        private SpriteRenderer _spriteRenderer;
        private float _currentTimeToInvisibility;
        private bool hasInvisibleSoundPlayed = false;

        public IdlePlayerState(Animator animator,
            PlayerSettings playerSettings,
            Player player,
            SignalBus signalBus,
            Rigidbody2D rigidbody2D,
            RunPlayerStateFactory runPlayerStateFactory,
            SneakPlayerStateFactory sneakPlayerStateFactory,
            ClimbPlayerStateFactory climbPlayerStateFactory,
            SpriteRenderer spriteRenderer,
            AudioManager audioManager
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _player = player;
            _signalBus = signalBus;
            _rigidbody2D = rigidbody2D;
            _runPlayerStateFactory = runPlayerStateFactory;
            _sneakPlayerStateFactory = sneakPlayerStateFactory;
            _climbPlayerStateFactory = climbPlayerStateFactory;
            _spriteRenderer = spriteRenderer;
            _audioManager = audioManager;
        }


        public void Init()
        {
            Debug.Log("entered idle state");
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("InAir", false);
            _currentTimeToInvisibility = _playerSettings.TimeToInvisibility;
            _signalBus.Subscribe<UseItemSignal>(SetInvisibleToFalse);
        }
        public void Deinit()
        {
            SetInvisible(false);
            Debug.Log("Deentered idle state");
            _signalBus.Unsubscribe<UseItemSignal>(SetInvisibleToFalse);
        }

        public void Tick()
        {

            if (Mathf.Abs(_player.Movement.x) > 0)
                _player.SetState(_runPlayerStateFactory.CreateState());

            if (_player.IsLadderAbove() && _player.IsLadderBelow())
                _player.SetState(_climbPlayerStateFactory.CreateState());
            else if (_player.IsLadderAbove()  || _player.IsLadderBelow())
            {
                if(Mathf.Abs(_player.Movement.y) >= 0.01f)
                    _player.SetState(_climbPlayerStateFactory.CreateState());
                _rigidbody2D.gravityScale = 0f;
            }
            else
            {
                _rigidbody2D.gravityScale = _playerSettings.DefaultGravityScale;
            }
                
            CalculateAndChangeInvisibility();
        }

        private void MakeInvisibleSound()
        {
            if (_player.IsInvisible)
            {
                if (!hasInvisibleSoundPlayed)
                {
                    _audioManager.Play("invisible");
                    hasInvisibleSoundPlayed = true;
                }
            }
        }

        public void FixedTick()
        {
           
        }

        public void UseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<UseItemSignal>();
        }

        public void Stealth(InputAction.CallbackContext context)
        {
            if (Keyboard.current.leftShiftKey.wasPressedThisFrame)
                _player.SetState(_sneakPlayerStateFactory.CreateState());
        }

        public void ChooseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<ChooseItemSignal>();
        }

        private void CalculateAndChangeInvisibility()
        {
            MakeInvisibleSound();
            if (_currentTimeToInvisibility <= 0)
            {
                SetInvisible(true);
            } 
            else
            {
                _currentTimeToInvisibility -= Time.deltaTime;
                float alpha = (_currentTimeToInvisibility / _playerSettings.TimeToInvisibility) * 0.5f + 0.5f;
                _spriteRenderer.color = new Color(1, 1, 1, alpha);
            }
        }

        private void SetInvisibleToFalse()
        {
            SetInvisible(false);
        }

        private void SetInvisible(bool value)
        {
            if(value == true)
            {
                _currentTimeToInvisibility = 0;
                _spriteRenderer.color = new Color(1, 1, 1, 0.5f);
                _player.IsInvisible = true;
            }
            else
            {
                _currentTimeToInvisibility = _playerSettings.TimeToInvisibility;
                _spriteRenderer.color = new Color(1, 1, 1, 1);
                _player.IsInvisible = false;
            }
        }

        public class Factory : PlaceholderFactory<IdlePlayerState>
        {
        }
    }
}
