using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using Underground.Players.States.Factories;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using System;
using Underground.Items.Signals;
using Underground.Audio;

namespace Underground.Players.States
{
    public class ClimbPlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private Rigidbody2D _rigidbody2D;
        private SignalBus _signalBus;
        private AudioManager _audioManager;
        private RunPlayerStateFactory _runPlayerStateFactory;
        private IdlePlayerStateFactory _idlePlayerStateFactory;
        private FallPlayerStateFactory _fallPlayerStateFactory;
        private bool _onTopOfLadder = false;
        private bool hasInvisibleSoundPlayed = false;
        private SpriteRenderer _spriteRenderer;
        private float _currentTimeToInvisibility;

        public ClimbPlayerState(Animator animator,
            PlayerSettings playerSettings,
            Player player,
            Rigidbody2D rigidbody2D,
            SignalBus signalBus,
            RunPlayerStateFactory runPlayerStateFactory,
            IdlePlayerStateFactory idlePlayerStateFactory,
            FallPlayerStateFactory fallPlayerStateFactory,
            SpriteRenderer spriteRenderer,
            AudioManager audioManager
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _player = player;
            _runPlayerStateFactory = runPlayerStateFactory;
            _idlePlayerStateFactory = idlePlayerStateFactory;
            _fallPlayerStateFactory = fallPlayerStateFactory;
            _rigidbody2D = rigidbody2D;
            _signalBus = signalBus;
            _spriteRenderer = spriteRenderer;
            _audioManager = audioManager;
        }


        public void Init()
        {
            Debug.Log("entered climb state");
            _rigidbody2D.gravityScale = 0;
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("InAir", false);
            _currentTimeToInvisibility = _playerSettings.TimeToInvisibility;
            _signalBus.Subscribe<UseItemSignal>(SetInvisibleToFalse);
        }
        public void Deinit()
        {
            _rigidbody2D.gravityScale = _playerSettings.DefaultGravityScale;
            _signalBus.Unsubscribe<UseItemSignal>(SetInvisibleToFalse);
            SetInvisible(false);
        }

        public void Tick()
        {
            if (!_player.IsLadderAbove() && _player.IsLadderBelow() && !_player.IsLadderBelow2())
                _onTopOfLadder = true;
            else
                _onTopOfLadder = false;

            if (!_player.IsLadderAbove() && !_player.IsLadderBelow() )
            {
                if (!_player.IsPlayerGrounded())
                    _player.SetState(_fallPlayerStateFactory.CreateState());
                else
                    _player.SetState(_idlePlayerStateFactory.CreateState());
            }

            if (Mathf.Abs(_player.Movement.x) < 0.1f && Mathf.Abs(_player.Movement.y) < 0.1f)
                CalculateAndChangeInvisibility();
            else
                SetInvisible(false);
        }
        public void FixedTick()
        {
            Move();
        }

        private void Move()
        {
            if (_onTopOfLadder && _player.Movement.y > 0)
                _player.Movement = new Vector2(_player.Movement.x, 0);
            _rigidbody2D.MovePosition(_rigidbody2D.position + new Vector2(_player.Movement.x * _playerSettings.RunSpeed * Time.fixedDeltaTime,
                                                                            _player.Movement.y * _playerSettings.ClimbSpeed * Time.fixedDeltaTime));
        }

        public void UseItem(InputAction.CallbackContext context)
        {

        }

        public void Stealth(InputAction.CallbackContext context)
        {

        }

        private void MakeInvisibleSound()
        {
            if (_player.IsInvisible)
            {
                if (!hasInvisibleSoundPlayed)
                {
                    _audioManager.Play("invisible");
                    hasInvisibleSoundPlayed = true;
                }
            }
        }

        public void ChooseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<ChooseItemSignal>();
        }

        private void CalculateAndChangeInvisibility()
        {
            MakeInvisibleSound();
            if (_currentTimeToInvisibility <= 0)
                SetInvisible(true);
            else
            {
                _currentTimeToInvisibility -= Time.deltaTime;
                float alpha = (_currentTimeToInvisibility / _playerSettings.TimeToInvisibility) * 0.5f + 0.5f;
                _spriteRenderer.color = new Color(1, 1, 1, alpha);
            }
        }

        private void SetInvisibleToFalse()
        {
            SetInvisible(false);
        }

        private void SetInvisible(bool value)
        {
            if (value == true)
            {
                _currentTimeToInvisibility = 0;
                _spriteRenderer.color = new Color(1, 1, 1, 0.5f);
                _player.IsInvisible = true;
            }
            else
            {
                _currentTimeToInvisibility = _playerSettings.TimeToInvisibility;
                _spriteRenderer.color = new Color(1, 1, 1, 1);
                _player.IsInvisible = false;
            }
        }

        public class Factory : PlaceholderFactory<ClimbPlayerState>
        {
        }
    }
}
