using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using Underground.Players.States.Factories;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using System;
using Underground.Items.Signals;
using Underground.Audio;
using Underground.Audio.SoundMaps;

namespace Underground.Players.States
{
    public class FallPlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private Rigidbody2D _rigidbody2D;
        private SignalBus _signalBus;
        private RunPlayerStateFactory _runPlayerStateFactory;
        private IdlePlayerStateFactory _idlePlayerStateFactory;
        private ClimbPlayerStateFactory _climbPlayerStateFactory;
        private AudioManager _audioManager;
        private SoundMapAgent _soundMapAgent;
        private float _timeInair;

        public FallPlayerState(Animator animator,
            PlayerSettings playerSettings,
            Player player,
            Rigidbody2D rigidbody2D,
            SignalBus signalBus,
            AudioManager audioManager,
            SoundMapAgent soundMapAgent,
            RunPlayerStateFactory runPlayerStateFactory,
            IdlePlayerStateFactory idlePlayerStateFactory,
            ClimbPlayerStateFactory climbPlayerStateFactory
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _player = player;
            _runPlayerStateFactory = runPlayerStateFactory;
            _idlePlayerStateFactory = idlePlayerStateFactory;
            _climbPlayerStateFactory = climbPlayerStateFactory;
            _rigidbody2D = rigidbody2D;
            _signalBus = signalBus;
            _audioManager = audioManager;
            _soundMapAgent = soundMapAgent;
        }


        public void Init()
        {
            Debug.Log("entered fall state");
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("InAir", true);
            _timeInair = 0f;
        }
        public void Deinit()
        {
            _animator.SetBool("InAir", false);
        }

        public void Tick()
        {
            _timeInair += Time.deltaTime;

            if (_player.IsLadderAbove())
                _player.SetState(_climbPlayerStateFactory.CreateState());
            else if (_player.IsPlayerGrounded() || _player.IsLadderBelow())
            {
                _audioManager.Play("drop", _timeInair);
                _soundMapAgent.AddNoise(_player.gameObject.transform.position, 6);
                _player.SetState(_idlePlayerStateFactory.CreateState());
            }

            
        }
        public void FixedTick()
        {
            Move();
        }

        private void Move()
        {
            _rigidbody2D.AddForce(new Vector2(_player.Movement.x * _playerSettings.SneakSpeed * Time.fixedDeltaTime * 2500,0));
        }

        public void UseItem(InputAction.CallbackContext context)
        {

        }

        public void Stealth(InputAction.CallbackContext context)
        {

        }

        public void ChooseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<ChooseItemSignal>();
        }



        public class Factory : PlaceholderFactory<FallPlayerState>
        {
        }
    }
}
