using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using Underground.Players.States.Factories;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using Underground.Items.Traps.Signals;
using Underground.Players.Signals;
using Underground.Audio;

namespace Underground.Players.States
{
    public class DeadPlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private SignalBus _signalBus;
        private AudioManager _audioManager;

        public DeadPlayerState(Animator animator,
            PlayerSettings playerSettings,
            Player player,
            SignalBus signalBus,
            AudioManager audioManager
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _player = player;
            _signalBus = signalBus;
            _audioManager = audioManager;
        }


        public void Init()
        {
            Debug.Log("entered dead state");
            _signalBus.Fire<PlayerDiedSignal>();
            _animator.SetBool("Dead", true);
            _audioManager.Play("death");

        }
        public void Deinit()
        {
            _animator.SetBool("Dead", false);
        }

        public void Tick()
        {}
        public void FixedTick()
        {}

        public void SetTrap(InputAction.CallbackContext context)
        {}

        public void Stealth(InputAction.CallbackContext context)
        {}

        public void ChooseTrap(InputAction.CallbackContext context)
        {}

        public void UseItem(InputAction.CallbackContext context)
        {
            
        }

        public void ChooseItem(InputAction.CallbackContext context)
        {
           
        }

        public class Factory : PlaceholderFactory<DeadPlayerState>
        {
        }
    }
}
