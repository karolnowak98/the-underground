using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;
using Underground.Players.States.Factories;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using Underground.Items.Traps.Signals;
using System;
using Underground.Audio;
using Underground.Items.Signals;
using Underground.Audio.SoundMaps;

namespace Underground.Players.States
{
    public class SneakPlayerState : IPlayerState
    {
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private Player _player;
        private SignalBus _signalBus;
        private Rigidbody2D _rigidbody2D;
        private AudioManager _audioManager;
        private RunPlayerStateFactory _runPlayerStateFactory;
        private IdlePlayerStateFactory _idlePlayerStateFactory;
        private FallPlayerStateFactory _fallPlayerStateFactory;
        private ClimbPlayerStateFactory _climbPlayerStateFactory;
        private SoundMapAgent _soundMapAgent;
        public SneakPlayerState(Animator animator,
            PlayerSettings playerSettings,
            Player player,
            SignalBus signalBus,
            Rigidbody2D rigidbody2D,
            AudioManager audioManager,
            SoundMapAgent soundMapAgent,
            RunPlayerStateFactory runPlayerStateFactory,
            IdlePlayerStateFactory idlePlayerStateFactory,
            FallPlayerStateFactory fallPlayerStateFactory,
            ClimbPlayerStateFactory climbPlayerStateFactory
            )
        {
            _animator = animator;
            _playerSettings = playerSettings;
            _player = player;
            _signalBus = signalBus;
            _audioManager = audioManager;
            _soundMapAgent = soundMapAgent;
            _runPlayerStateFactory = runPlayerStateFactory;
            _idlePlayerStateFactory = idlePlayerStateFactory;
            _fallPlayerStateFactory = fallPlayerStateFactory;
            _climbPlayerStateFactory = climbPlayerStateFactory;
            _rigidbody2D = rigidbody2D;
        }


        public void Init()
        {
            Debug.Log("entered sneak state");
            _animator.SetFloat("Speed", 0f);
            _animator.SetBool("InAir", false);
        }
        public void Deinit()
        {
            _audioManager.StopPlaying();
        }

        public void Tick()
        {
            if (Mathf.Abs(_player.Movement.x) > 0)
            {
                _audioManager.Play("silentStep");
                _soundMapAgent.AddNoise(_player.transform.position, _playerSettings.SneakLoudness);
            }
            else
                _audioManager.StopPlaying();

            if (_player.IsLadderAbove() && _player.IsLadderBelow())
                _player.SetState(_climbPlayerStateFactory.CreateState());
            else if (_player.IsLadderAbove() || _player.IsLadderBelow())
            {
                if (Mathf.Abs(_player.Movement.y) > 0.01f)
                    _player.SetState(_climbPlayerStateFactory.CreateState());

                _rigidbody2D.gravityScale = 0f;
            }
            else if (!_player.IsPlayerGrounded() && !_player.IsLadderBelow())
                _player.SetState(_fallPlayerStateFactory.CreateState());
        }
        public void FixedTick()
        {
            Move();
        }

        private void Move()
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + new Vector2(_player.Movement.x * _playerSettings.SneakSpeed * Time.fixedDeltaTime, 0));
        }

        public void UseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<UseItemSignal>();
        }

        public void Stealth(InputAction.CallbackContext context)
        {
            if(Keyboard.current.leftShiftKey.wasReleasedThisFrame)
            {
                if (Mathf.Abs(_player.Movement.x) > 0)
                    _player.SetState(_runPlayerStateFactory.CreateState());
                else
                    _player.SetState(_idlePlayerStateFactory.CreateState());
            }
        }

        public void ChooseItem(InputAction.CallbackContext context)
        {
            if (context.performed)
                _signalBus.Fire<ChooseItemSignal>();
        }



        public class Factory : PlaceholderFactory<SneakPlayerState>
        {
        }
    }
}
