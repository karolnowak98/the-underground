using UnityEngine.InputSystem;

namespace Underground.Players.States
{
    public interface IPlayerState
    {
        public void Init();
        public void Tick();
        public void Deinit();
        public void FixedTick();
        public void UseItem(InputAction.CallbackContext context);
        public void ChooseItem(InputAction.CallbackContext context);
        public void Stealth(InputAction.CallbackContext context);

    }
}
