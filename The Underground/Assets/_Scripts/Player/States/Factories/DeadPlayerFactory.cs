namespace Underground.Players.States.Factories
{
    public class DeadPlayerStateFactory
    {
        readonly DeadPlayerState.Factory deadPlayerStateFactory;

        public DeadPlayerStateFactory(DeadPlayerState.Factory deadPlayerStateFactory)
        {
            this.deadPlayerStateFactory = deadPlayerStateFactory;
        }

        public DeadPlayerState CreateState()
        {
            DeadPlayerState deadPlayerState = deadPlayerStateFactory.Create();
            return deadPlayerState;
        }
    }
}
