namespace Underground.Players.States.Factories
{
    public class IdlePlayerStateFactory 
    {
        readonly IdlePlayerState.Factory idlePlayerStateFactory;

        public IdlePlayerStateFactory(IdlePlayerState.Factory idlePlayerStateFactory)
        {
            this.idlePlayerStateFactory = idlePlayerStateFactory;
        }

        public IdlePlayerState CreateState()
        {
            IdlePlayerState idlePlayerState = idlePlayerStateFactory.Create();
            return idlePlayerState;
        }
    }
}
