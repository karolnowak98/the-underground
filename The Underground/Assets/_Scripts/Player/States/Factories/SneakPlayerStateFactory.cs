namespace Underground.Players.States.Factories
{
    public class SneakPlayerStateFactory
    {
        readonly SneakPlayerState.Factory sneakPlayerStateFactory;

        public SneakPlayerStateFactory(SneakPlayerState.Factory sneakPlayerStateFactory)
        {
            this.sneakPlayerStateFactory = sneakPlayerStateFactory;
        }

        public SneakPlayerState CreateState()
        {
            SneakPlayerState sneakPlayerState = sneakPlayerStateFactory.Create();
            return sneakPlayerState;
        }
    }
}