namespace Underground.Players.States.Factories
{
    public class ClimbPlayerStateFactory
    {
        readonly ClimbPlayerState.Factory climbPlayerStateFactory;

        public ClimbPlayerStateFactory(ClimbPlayerState.Factory climbPlayerStateFactory)
        {
            this.climbPlayerStateFactory = climbPlayerStateFactory;
        }

        public ClimbPlayerState CreateState()
        {
            ClimbPlayerState climbPlayerState = climbPlayerStateFactory.Create();
            return climbPlayerState;
        }
    }
}
