namespace Underground.Players.States.Factories
{
    public class FallPlayerStateFactory
    {
        readonly FallPlayerState.Factory fallPlayerStateFactory;

        public FallPlayerStateFactory(FallPlayerState.Factory fallPlayerStateFactory)
        {
            this.fallPlayerStateFactory = fallPlayerStateFactory;
        }

        public FallPlayerState CreateState()
        {
            FallPlayerState fallPlayerState = fallPlayerStateFactory.Create();
            return fallPlayerState;
        }
    }
}

