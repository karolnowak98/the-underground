namespace Underground.Players.States.Factories
{
    public class RunPlayerStateFactory
    {
        readonly RunPlayerState.Factory runPlayerStateFactory;

        public RunPlayerStateFactory(RunPlayerState.Factory runPlayerStateFactory)
        {
            this.runPlayerStateFactory = runPlayerStateFactory;
        }

        public RunPlayerState CreateState()
        {
            RunPlayerState runPlayerState = runPlayerStateFactory.Create();
            return runPlayerState;
        }
    }
}