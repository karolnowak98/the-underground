using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class RunPlayerStateInstaller : MonoInstaller<RunPlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<RunPlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallRunPlayerStateFactory).AsSingle();
        }

        private void InstallRunPlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<RunPlayerStateFactory>().AsSingle();
            subContainer.BindFactory<RunPlayerState, RunPlayerState.Factory>().AsSingle();
        }
    }
}