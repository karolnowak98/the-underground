using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class SneakPlayerStateInstaller : MonoInstaller<SneakPlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<SneakPlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallSneakPlayerStateFactory).AsSingle();
        }

        private void InstallSneakPlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<SneakPlayerStateFactory>().AsSingle();
            subContainer.BindFactory<SneakPlayerState, SneakPlayerState.Factory>().AsSingle();
        }
    }
}