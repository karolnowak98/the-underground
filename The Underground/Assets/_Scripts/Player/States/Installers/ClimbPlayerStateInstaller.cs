using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class ClimbPlayerStateInstaller : MonoInstaller<ClimbPlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<ClimbPlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallClimbPlayerStateFactory).AsSingle();
        }

        private void InstallClimbPlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<ClimbPlayerStateFactory>().AsSingle();
            subContainer.BindFactory<ClimbPlayerState, ClimbPlayerState.Factory>().AsSingle();
        }
    }
}