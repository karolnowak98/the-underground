using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class DeadPlayerStateInstaller : MonoInstaller<DeadPlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<DeadPlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallDeadPlayerStateFactory).AsSingle();
        }

        private void InstallDeadPlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<DeadPlayerStateFactory>().AsSingle();
            subContainer.BindFactory<DeadPlayerState, DeadPlayerState.Factory>().AsSingle();
        }
    }
}