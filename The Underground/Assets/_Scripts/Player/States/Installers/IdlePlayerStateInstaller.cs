using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class IdlePlayerStateInstaller : MonoInstaller<IdlePlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IdlePlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallIdlePlayerStateFactory).AsSingle();
        }

        private void InstallIdlePlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<IdlePlayerStateFactory>().AsSingle();
            subContainer.BindFactory<IdlePlayerState, IdlePlayerState.Factory>().AsSingle();
        }
    }
}