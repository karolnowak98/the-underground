using Underground.Players.States.Factories;
using Zenject;

namespace Underground.Players.States.Installers
{
    public class FallPlayerStateInstaller : MonoInstaller<FallPlayerStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<FallPlayerStateFactory>().FromSubContainerResolve().ByMethod(InstallFallPlayerStateFactory).AsSingle();
        }

        private void InstallFallPlayerStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<FallPlayerStateFactory>().AsSingle();
            subContainer.BindFactory<FallPlayerState, FallPlayerState.Factory>().AsSingle();
        }
    }
}