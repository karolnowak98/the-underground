using UnityEngine;
using System;

namespace Underground.Players.Settings
{
    [Serializable]
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Data/PlayerSettings", order = 1)]
    public class PlayerSettings : ScriptableObject
    {
        public float RunSpeed;
        public float ClimbSpeed;
        public float SneakSpeed;
        public float DefaultGravityScale;
        public float RunLoudness;
        public float SneakLoudness;
        public LayerMask LadderLayer;
        public LayerMask GroundLayer;
        public float TimeToInvisibility;
    }
}