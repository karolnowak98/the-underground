using UnityEngine;
using Zenject;
using System;
using Underground.Players.Settings;

namespace Underground.Players.Installers
{
    [Serializable]
    [CreateAssetMenu(fileName = "PlayerSettingsInstaller", menuName = "Installers/PlayerSettingsInstaller", order = 1)]
    public class PlayerSettingsInstaller : ScriptableObjectInstaller
    {
        public PlayerSettings PlayerSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(PlayerSettings);
        }
    }
}
