using Underground.Players.Signals;
using Zenject;

namespace Underground.Players.Installers
{
    public class PlayerInstaller : MonoInstaller<PlayerInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<Player>().FromSubContainerResolve().ByMethod(InstallPlayer).AsSingle().NonLazy();
            Container.DeclareSignal<PlayerDiedSignal>();
        }

        private void InstallPlayer(DiContainer subContainer)
        {
            subContainer.Bind<Player>().AsSingle().NonLazy();
        }
    }
}