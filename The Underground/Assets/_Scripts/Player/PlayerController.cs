using Underground.FieldOfViews;
using Underground.InteractableObjects;
using Underground.Items.Traps.Signals;
using Underground.Audio;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using Underground.Units.Enemies;
using Underground.Items.Signals;

namespace Underground.Game
{
    public class PlayerController : MonoBehaviour, IInteractor
    {
        [SerializeField] private float _moveSpeed = 10f;
        [SerializeField] private float _climbSpeed = 5f;
        [SerializeField] private float _stealthSpeed = 3f;
        [SerializeField] private LayerMask _ladderLayer;
        [SerializeField] private LayerMask _groundLayer;
        [SerializeField] private LayerMask _enemyLayer;

        private Rigidbody2D _rb;
        private CapsuleCollider2D _capsuleCollider;
        private Vector2 _movement;
        private SignalBus signalBus;
        private Animator _animator;
        private FieldOfView fieldOfView;
        private AudioManager _audioManager;

        private bool _isPlayerStealth = false;
        private bool _isPlayerOnLadder = false;
        private bool _isPlayerGrounded = false;
        private bool _isPlayerFacingRight = true;  // for determining which way the player is currently facing
        private bool _isPlayerRunning = false;

        public IInteractable ClosestInteractable { get; set; }

        [Inject]
        private void Construct(SignalBus signalBus,
            FieldOfView fieldOfView)
        {
            this.signalBus = signalBus;
            this.fieldOfView = fieldOfView;
            
        }

        private readonly float _defaultGravityValue = 50f;
        [SerializeField]
        private float _raycastDistance = 0.01f;


        void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            _capsuleCollider = GetComponent<CapsuleCollider2D>();
            _animator = GetComponent<Animator>();
            _rb.gravityScale = _defaultGravityValue;
            _audioManager = GetComponent<AudioManager>();
            fieldOfView.Init();
        }

        private void FixedUpdate()
        {
            float hSpeed = 0;

            if (_isPlayerGrounded)
                hSpeed = _isPlayerStealth ? _stealthSpeed : _moveSpeed;
            else
                hSpeed = _moveSpeed / 2;


            float hMovement = _movement.x * hSpeed * Time.fixedDeltaTime;
            float vMovement = 0;

            if (IsLadderAbove() && _movement.y >= 0)
            {
                _isPlayerOnLadder = true;
                vMovement = _movement.y * _climbSpeed * Time.fixedDeltaTime;
                _rb.gravityScale = 0;
            }
            else if (IsLadderBelow2())
            {
                _isPlayerOnLadder = true;
                if (IsLadderBelow() && _movement.y <= 0)
                    vMovement = _movement.y * _climbSpeed * Time.fixedDeltaTime;
                _rb.gravityScale = 0;
            }
            else
            {
                _isPlayerOnLadder = false;
                vMovement = 0;
                _rb.gravityScale = _defaultGravityValue;
            }

            _rb.MovePosition(_rb.position + new Vector2(hMovement, vMovement));
        }

        private void Update()
        {
            CheckIfPlayerGrounded();
            CheckIfPlayerRunning();
            UpdateAnimation();
            UpdateSounds();
            fieldOfView.Tick();
        }



        private void UpdateSounds()
        {
            if(_isPlayerRunning)
            {
                _audioManager.Play("step");
                DistorbEnemiesAround(5f);
                
            }
                
            else if(_isPlayerStealth)
            {
                if(Mathf.Abs(_movement.x) > 0)
                {
                    _audioManager.Play("silentStep");
                    DistorbEnemiesAround(3f);
                }
            }      
        }

        private void DistorbEnemiesAround(float distance)
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, distance, _enemyLayer);
            if (colliders.Length > 0)
            {
                foreach (Collider2D col in colliders)
                    col.gameObject.GetComponent<BasicEnemy>().Target = this.gameObject;
            }
        }

        private void CheckIfPlayerRunning()
        {
            if (_isPlayerGrounded && !_isPlayerStealth && Mathf.Abs(_movement.x) > 0)
                _isPlayerRunning = true;
            else
                _isPlayerRunning = false;
        }

        private void CheckIfPlayerGrounded()
        {
            _isPlayerGrounded = false;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(_capsuleCollider.bounds.min, 0.2f, _groundLayer);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    _isPlayerGrounded = true;
                }
            }
        }

        private void UpdateAnimation()
        {
            if(_isPlayerGrounded)
            {
                _animator.SetFloat("Speed", Mathf.Abs(_movement.x));
                _animator.SetBool("InAir", false);
                
            } 
            else if(_isPlayerOnLadder)
            {
                _animator.SetFloat("Speed", 0);
                _animator.SetBool("InAir", false);
            }  
            else
            {
                _animator.SetFloat("Speed", 0);
                _animator.SetBool("InAir", true);
            }
                
            if (_isPlayerFacingRight && _movement.x < 0)
                FlipPlayer();
            else if(!_isPlayerFacingRight && _movement.x > 0)
                FlipPlayer();
        }

        private void FlipPlayer()
        {
            _isPlayerFacingRight = !_isPlayerFacingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        private bool IsLadderAbove()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y, 0),Vector2.up, _raycastDistance, _ladderLayer);
            return hitInfo.collider != null;
        }

        private bool IsLadderBelow()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y, 0), Vector2.down, _raycastDistance, _ladderLayer);
            return hitInfo.collider != null;
        }

        private bool IsLadderBelow2()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y-0.01f, 0), Vector2.down, _raycastDistance, _ladderLayer);
            return hitInfo.collider != null;
        }

        public void Move(InputAction.CallbackContext context)
        {
            _movement = context.ReadValue<Vector2>();
        }

        public void UseItem(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                if((_isPlayerGrounded) && (!_isPlayerOnLadder))
                {
                    signalBus.Fire<UseItemSignal>();
                }  
            }
        }

        public void ChooseItem(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                signalBus.Fire<ChooseItemSignal>();
            }
        }

        public void Stealth(InputAction.CallbackContext ctx)
        {
            if (Keyboard.current.leftCtrlKey.wasPressedThisFrame)
                _isPlayerStealth = true;
            else if(Keyboard.current.leftCtrlKey.wasReleasedThisFrame)
                _isPlayerStealth = false;
        }

        public void Interact(InputAction.CallbackContext context)
        {

            if (ClosestInteractable != null && Keyboard.current.iKey.wasPressedThisFrame)
                ClosestInteractable.Interact(gameObject);
        }
    }
}

