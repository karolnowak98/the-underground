using System.Collections;
using Underground.FieldOfViews;
using UnityEngine;
using Underground.Players.Settings;
using Underground.Players.States;
using Underground.Players.States.Factories;
using Zenject;
using UnityEngine.InputSystem;
using Underground.InteractableObjects;
using Underground.UI.InGame.Healths;
using System;


namespace Underground.Players
{
    public class Player : MonoBehaviour, IInteractor
    {
        private IPlayerState _currentState;
        private Animator _animator;
        private PlayerSettings _playerSettings;
        private SignalBus _signalBus;
        private FieldOfView _fieldOfView;
        private CapsuleCollider2D _capsuleCollider;
        private PlayerHealth _playerHealth;
        private DeadPlayerStateFactory _deadPlayerStateFactory;
        private bool _isPaused;
        private Rigidbody2D _rigidbody2D;

        public bool _isPlayerFacingRight = true;
        private bool _isPlayerDead = false;
        public Vector2 Movement { get; set; }
        public IInteractable ClosestInteractable { get; set; }
        public bool IsInvisible { get; set; }

        [Inject]
        private void Construct(
            IdlePlayerStateFactory idlePlayerStateFactory,
            DeadPlayerStateFactory deadPlayerStateFactory,
            Animator animator,
            PlayerSettings playerSettings,
            SignalBus signalBus,
            FieldOfView fieldOfView,
            CapsuleCollider2D capsuleCollider,
            PlayerHealth playerHealth,
            Rigidbody2D rigidbody2D)
        {
            _deadPlayerStateFactory = deadPlayerStateFactory;
            _currentState = idlePlayerStateFactory.CreateState();
            _animator = animator;
            _playerSettings = playerSettings;
            _signalBus = signalBus;
            _fieldOfView = fieldOfView;
            _capsuleCollider = capsuleCollider;
            _playerHealth = playerHealth;
            _rigidbody2D = rigidbody2D;
        }

        public void Init()
        {
            _currentState.Init();
            _fieldOfView.Init();
            _playerHealth.HealthDecreased += TakeHit;
            _playerHealth.HealthAtZero += OnDead;
        }

        public void Tick()
        {
            if (_isPaused)
                return;

            _currentState.Tick();
            _fieldOfView.Tick();
            if (!_isPlayerDead)
            {
                UpdateRotation();
            }
        }

        private void UpdateRotation()
        {
            if ((_isPlayerFacingRight && Movement.x < 0) || (!_isPlayerFacingRight && Movement.x > 0))
            {
                _isPlayerFacingRight = !_isPlayerFacingRight;
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
        }

        public void FixedTick()
        {
            _currentState.FixedTick();
        }

        public void Deinit()
        {
            _playerHealth.HealthDecreased -= TakeHit;
            _playerHealth.HealthAtZero -= OnDead;
        }

        public void SetState(IPlayerState nextPlayerState)
        {

            _currentState.Deinit();
            _currentState = nextPlayerState;
            _currentState.Init();
        }

        public void Move(InputAction.CallbackContext context)
        {
            Movement = context.ReadValue<Vector2>();
        }

        public void SetTrap(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _currentState.UseItem(context);
            }
        }

        public void ChooseTrap(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _currentState.ChooseItem(context);
            }
        }

        public void Stealth(InputAction.CallbackContext context)
        {
            _currentState.Stealth(context);
        }

        public void Interact(InputAction.CallbackContext context)
        {
            if(!_isPlayerDead)
            {
                if (ClosestInteractable != null && Keyboard.current.iKey.wasPressedThisFrame)
                    ClosestInteractable.Interact(gameObject);
            }
        }

        public bool IsLadderAbove()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y, 0), Vector2.up, 0.1f, _playerSettings.LadderLayer );
            return hitInfo.collider != null;
        }

        public bool IsLadderBelow()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y - 0.1f, 0), Vector2.down, 0.01f, _playerSettings.LadderLayer);
            return hitInfo.collider != null;
        }

        public bool IsLadderBelow2()
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector3(transform.position.x, _capsuleCollider.bounds.min.y, 0), Vector2.down, 0.01f, _playerSettings.LadderLayer);
            return hitInfo.collider != null;
        }

        public bool IsPlayerGrounded()
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll( new Vector2(transform.position.x, _capsuleCollider.bounds.min.y), 0.1f, _playerSettings.GroundLayer);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    return true;
                }
            }
            return false;
        }

        public void TakeHit()
        {
            if (!_isPlayerDead)
            {
                StartCoroutine("ShowTakeHitAnimation");
            }
        }

        private IEnumerator ShowTakeHitAnimation()
        {
            _animator.SetBool("Hit", true);
            yield return new WaitForSeconds(0.2f);
            _animator.SetBool("Hit",false);
        }

        public void OnDead()
        {
            _isPlayerDead = true;
            SetState(_deadPlayerStateFactory.CreateState());
        }

        public void SetActiveAnimator(bool value)
        {
            if (_animator != null)
                _animator.enabled = value;
        }

        public void SetPause(bool value)
        {
            _isPaused = value;
            _rigidbody2D.constraints = (value == true) 
                ? (RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation) 
                : (RigidbodyConstraints2D.FreezeRotation);
        }
    }
}

