using Underground.FieldOfViews;
using Underground.Maps;
using UnityEngine;
using Zenject;

public class FieldOfViewDisplay : MonoBehaviour
{
    [SerializeField] private bool showGizmos;
    private FieldOfView fieldOfView;
    private MapFacade mapFacade;

    [Inject]
    private void Construct(
        FieldOfView fieldOfView,
        MapFacade mapFacade)
    {
        this.fieldOfView = fieldOfView;
        this.mapFacade = mapFacade;
    }

    private void OnDrawGizmos()
    {
        if (fieldOfView == null || showGizmos == false || fieldOfView.RaysVectors == null)
            return;

        for (int i = 0; i < fieldOfView.RaysVectors.Count; i++)
        {
            Vector3 rayVector = fieldOfView.RaysVectors[i];
            Gizmos.DrawLine(
                fieldOfView.FieldOfViewStartPoint.position,
                fieldOfView.FieldOfViewStartPoint.position + rayVector);
        }
    }
}
