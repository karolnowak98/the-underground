using UnityEngine;
using System;

namespace Underground.FieldOfViews.Settings
{
    [Serializable]
    [CreateAssetMenu(fileName = "FieldOfViewSettings", menuName = "Data/FieldOfViewSettings", order = 1)]
    public class FieldOfViewSettings : ScriptableObject
    {
        public float FieldOfViewDistance;
        public float FieldOfViewAngle;
        public float FieldOfViewGapBetweenRays;
        public bool SaveRayVectors;
    }
}
