using System.Collections.Generic;
using Underground.FieldOfViews.Settings;
using UnityEngine;
using Zenject;

namespace Underground.FieldOfViews
{
    public class FieldOfView
    {
        private float angle;
        private float gapBetweenRays;
        private float angleBetweenRays;
        private float startAngle;
        private int numberOfRays;
        private bool saveRaysVectors;

        public List<GameObject> VisibleObjects { get; private set; }
        public Transform FieldOfViewStartPoint { get; private set; }
        public float FieldOfViewDistance { get; private set; }
        public List<Vector2> RaysVectors { get; private set; }

        public FieldOfView(
            FieldOfViewSettings fieldOfViewSettings,
            [Inject(Id = "FieldOfViewStartPoint")]Transform fieldOfViewStartPoint)
        {
            saveRaysVectors = fieldOfViewSettings.SaveRayVectors;
            FieldOfViewStartPoint = fieldOfViewStartPoint;
            FieldOfViewDistance = fieldOfViewSettings.FieldOfViewDistance;
            angle = fieldOfViewSettings.FieldOfViewAngle;
            gapBetweenRays = fieldOfViewSettings.FieldOfViewGapBetweenRays;
        }

        public void Init()
        {
            angleBetweenRays = Mathf.Rad2Deg * (gapBetweenRays / FieldOfViewDistance);
            numberOfRays = Mathf.FloorToInt(angle / angleBetweenRays);
            startAngle = angle / -2f;
            VisibleObjects = new List<GameObject>();

            if (saveRaysVectors)
                RaysVectors = new List<Vector2>();
        }

        public void Tick()
        {
            VisibleObjects.Clear();
            if (saveRaysVectors)
                RaysVectors.Clear();

            for (int i = 0; i <= numberOfRays; i++)
            {
                float rotationAngle = FieldOfViewStartPoint.parent.eulerAngles.y;

                Vector3 direction = (Quaternion.Euler(0, 0, startAngle + angleBetweenRays * i - 90f + rotationAngle) * FieldOfViewStartPoint.up).normalized;
                RaycastHit2D[] hits = Physics2D.RaycastAll(FieldOfViewStartPoint.position, direction, FieldOfViewDistance);
                float distanceToClosestVisionBlocker = FieldOfViewDistance;

                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.transform.CompareTag("Wall"))
                    {
                        float distance = Vector3.Distance(hit.point, FieldOfViewStartPoint.position);
                        if (distance < distanceToClosestVisionBlocker)
                            distanceToClosestVisionBlocker = distance;
                    }
                }

                if (saveRaysVectors)
                    RaysVectors.Add(direction * (distanceToClosestVisionBlocker + 0.2f));

                foreach (RaycastHit2D hit in hits)
                {
                    if (Vector3.Distance(hit.transform.position, FieldOfViewStartPoint.position) < distanceToClosestVisionBlocker)
                    {
                        if (VisibleObjects.Contains(hit.transform.gameObject) == false)
                            VisibleObjects.Add(hit.transform.gameObject);
                    }
                }
            }
        }
    }
}
