using UnityEngine;
using Zenject;

namespace Underground.FieldOfViews.Installers
{
    public class FieldOfViewInstaller : MonoInstaller<FieldOfViewInstaller>
    {
        public Transform FieldOfViewStartPoint;

        public override void InstallBindings()
        {
            Container.Bind<FieldOfView>().FromSubContainerResolve().ByMethod(InstallFieldOfView).AsSingle();
        }

        private void InstallFieldOfView(DiContainer subContainer)
        {
            subContainer.Bind<FieldOfView>().AsSingle();
            subContainer.BindInstance(FieldOfViewStartPoint).WithId("FieldOfViewStartPoint").AsSingle();
        }
    }
}
