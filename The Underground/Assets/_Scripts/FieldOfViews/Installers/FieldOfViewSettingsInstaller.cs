using UnityEngine;
using Zenject;
using System;
using Underground.FieldOfViews.Settings;

namespace Underground.FieldOfViews.Installers
{
    [Serializable]
    [CreateAssetMenu(fileName = "FieldOfViewSettingsInstaller", menuName = "Installers/FieldOfViewSettingsInstaller", order = 1)]
    public class FieldOfViewSettingsInstaller : ScriptableObjectInstaller
    {
        public FieldOfViewSettings FieldOfViewSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(FieldOfViewSettings);
        }
    }
}
