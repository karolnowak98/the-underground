using Zenject;

namespace Underground.Pause.Installers
{
    public class PauseManagerInstaller : MonoInstaller<PauseManagerInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<PauseManager>().AsSingle();
        }
    }
}
