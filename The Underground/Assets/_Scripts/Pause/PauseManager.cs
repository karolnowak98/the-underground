using UnityEngine.InputSystem;
using Zenject;
using Underground.Game.States.Signals;

namespace Underground.Pause
{
    public class PauseManager
    {
        private SignalBus signalBus;
        private bool isPause;

        public PauseManager(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        public void Tick()
        {
            if (Keyboard.current.pKey.isPressed && Keyboard.current.pKey.wasPressedThisFrame)
            {
                if (isPause)
                    signalBus.Fire(new ChangeToInGameStateSignal());
                else
                    signalBus.Fire(new ChangeToPauseStateSignal());
                isPause = !isPause;
            }
        }
    }
}
