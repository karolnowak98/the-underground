using System.Collections;
using System.Collections.Generic;
using Underground.Items.Potions.Signals;
using Underground.Players;
using Underground.UI.InGame.Healths;
using UnityEngine;
using Zenject;

namespace Underground.Items.Potions
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class HealthPotion : MonoBehaviour
    {
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                signalBus.Fire<PickHealthPotionSignal>();
                Destroy(this.gameObject);
            }
        }
    }
}