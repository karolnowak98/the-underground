using System.Collections;
using System.Collections.Generic;
using Underground.Items.Armors.Signals;
using UnityEngine;
using Zenject;

namespace Underground.Items.Armors
{
    public class Armor : MonoBehaviour
    {
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                signalBus.Fire<PickArmorSignal>();
                Destroy(this.gameObject);
            }
        }
    }
}