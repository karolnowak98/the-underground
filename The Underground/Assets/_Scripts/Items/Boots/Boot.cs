using System.Collections;
using System.Collections.Generic;
using Underground.Items.Boots.Signals;
using UnityEngine;
using Zenject;

namespace Underground.Items.Boots
{
    public class Boot : MonoBehaviour
    {
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                signalBus.Fire<PickBootsSignal>();
                Destroy(this.gameObject);
            }
        }
    }
}