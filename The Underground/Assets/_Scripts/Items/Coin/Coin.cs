using Underground.Items.Coins.Signals;
using Underground.Audio;
using UnityEngine;
using Zenject;
using System.Collections;

namespace Underground.Items.Coins
{
    public class Coin : MonoBehaviour
    {
        private SignalBus signalBus;
        private AudioManager audioManager;

        [Inject]
        private void Construct(SignalBus signalBus, AudioManager audioManager)
        {
            this.signalBus = signalBus;
            this.audioManager = audioManager;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                signalBus.Fire<PickUpCoinSignal>();
                audioManager.PlayClipAtPoint(gameObject.transform.position, "PickUpCoin");
                Destroy(gameObject);
            }
        }
    }
}
