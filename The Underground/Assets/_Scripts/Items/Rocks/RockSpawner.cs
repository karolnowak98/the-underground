using System.Collections;
using System.Collections.Generic;
using Underground.Players;
using UnityEngine;

namespace Underground.Items.Rocks
{
    public class RockSpawner
    {
        private readonly Rock.Factory rockFactory;
        private readonly Player player;

        public RockSpawner(Rock.Factory rockFactory, Player player)
        {
            this.rockFactory = rockFactory;
            this.player = player;
        }

        public GameObject SpawnRock()
        {
             //rockFactory.Create().transform.position = player.transform.position;
             return rockFactory.Create().gameObject;
        }
    }
}