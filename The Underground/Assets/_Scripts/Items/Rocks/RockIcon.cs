using System.Collections;
using System.Collections.Generic;
using Underground.Items.Rocks.Signals;
using UnityEngine;
using Zenject;

public class RockIcon : MonoBehaviour
{
    private SignalBus signalBus;

    [Inject]
    private void Construct(SignalBus signalBus)
    {
        this.signalBus = signalBus;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            signalBus.Fire<PickRockSignal>();
            Destroy(this.gameObject);
        }
    }
}
