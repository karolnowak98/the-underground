using System.Collections;
using System.Collections.Generic;
using Underground.Players;
using UnityEngine;
using Zenject;

namespace Underground.Items.Rocks
{
    public class RockController
    {
        private Vector2 direction;

        private Player player;
        private RockSpawner rockSpawner;
        private GameObject rock;
        private Rigidbody2D rb;

        [Inject]
        private void Construct(Player player, RockSpawner rockSpawner)
        {
            this.player = player;
            this.rockSpawner = rockSpawner;
        }

        public void Throw()
        {
            rock = rockSpawner.SpawnRock();
            rock.transform.position = player.gameObject.transform.position;
            rb = rock.GetComponent<Rigidbody2D>();

            float yDirection = Random.Range(2f,4f);

            if (player._isPlayerFacingRight)
            {
                direction = new Vector2(2f, yDirection);
            }
            else
            {
                direction = new Vector2(-2f, yDirection);
            }

            rb.AddForce(direction * 100f);
        }
    }
}