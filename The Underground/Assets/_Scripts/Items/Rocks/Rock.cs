using System.Collections;
using System.Collections.Generic;
using Underground.Audio;
using Underground.Audio.SoundMaps;
using Underground.Players;
using UnityEngine;
using Zenject;

namespace Underground.Items.Rocks
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Rock : MonoBehaviour
    {
        private Player player;
        private AudioManager audioManager;
        private SoundMapAgent soundMapAgent;
        private PolygonCollider2D pCollider;
        private float loudness;

        [Inject]
        private void Construct(Player player, SoundMapAgent soundMapAgent, AudioManager audioManager, PolygonCollider2D pCollider)
        {
            this.player = player;
            this.soundMapAgent = soundMapAgent;
            this.audioManager = audioManager;
            this.pCollider = pCollider;
        }

        private void Start()
        {
            Physics2D.IgnoreCollision(player.GetComponent<CapsuleCollider2D>(), pCollider);
            loudness = 8f;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if ((collision.gameObject.CompareTag("Wall")) || (collision.gameObject.CompareTag("Rock")))
            {
                int soundNumber = Random.Range(1, 3);
                audioManager.Play($"Hit{soundNumber}");
                soundMapAgent.AddNoise(gameObject.transform.position, loudness);
            }
        }

        public class Factory : PlaceholderFactory<Rock> { }
    }
}