using Underground.Audio;
using Underground.Items.Armors.Signals;
using Underground.Items.Boots.Signals;
using Underground.Items.Potions.Signals;
using Underground.Items.Rocks.Signals;
using Underground.Items.Traps.Signals;
using Underground.Items.Coins.Signals;
using Underground.UI.InGame.Healths;
using Underground.UI.InGame.Items;
using Underground.UI.InGame.Coins;
using UnityEngine;
using Zenject;
using Underground.Players.Settings;

namespace Underground.Items
{
    public class PickItemsController : MonoBehaviour
    {
        private SignalBus signalBus;
        private HealthBarController healthBarController;
        private ItemsController trapsController;
        private AudioManager audioManager;
        private PlayerSettings playerSettings;

        [Inject]
        private void Construct(SignalBus signalBus, HealthBarController healthBarController, ItemsController trapsController, PlayerSettings playerSettings)
        {
            this.signalBus = signalBus;
            this.healthBarController = healthBarController;
            this.trapsController = trapsController;
            this.playerSettings = playerSettings;
        }

        private void Start()
        {
            audioManager = GetComponent<AudioManager>();
        }

        private void OnEnable()
        {
            signalBus.Subscribe<PickBootsSignal>(AddBootsBonus);
            signalBus.Subscribe<PickRockSignal>(AddRock);
            signalBus.Subscribe<PickHealthPotionSignal>(HealPlayer);
            signalBus.Subscribe<PickArmorSignal>(AddHealthHearts);
            signalBus.Subscribe<PickBearTrapSignal>(AddBearTrap);
        }

        private void OnDisable()
        {
            signalBus.Unsubscribe<PickBootsSignal>(AddBootsBonus);
            signalBus.Unsubscribe<PickRockSignal>(AddRock);
            signalBus.Unsubscribe<PickHealthPotionSignal>(HealPlayer);
            signalBus.Unsubscribe<PickArmorSignal>(AddHealthHearts);
            signalBus.Unsubscribe<PickBearTrapSignal>(AddBearTrap);
        }

        private void AddRock()
        {
            audioManager.Play("PickRock");
            trapsController.AddRock(1);
        }

        private void AddBootsBonus()
        {
            audioManager.Play("PickBoots");
            playerSettings.RunSpeed += 1.5f;
            playerSettings.ClimbSpeed += 0.5f;
            playerSettings.SneakSpeed += 0.5f;
        }

        private void HealPlayer()
        {
            audioManager.Play("PickHealthPotion");
            healthBarController.Heal(1);
        }

        private void AddHealthHearts()
        {
            audioManager.Play("PickArmor");
            healthBarController.AddHealth(2);
        }

        private void AddBearTrap()
        {
            audioManager.Play("PickBearTrap");
            trapsController.AddTrap(1);
        }
    }
}