using Underground.Items.Armors.Signals;
using Underground.Items.Boots.Signals;
using Underground.Items.Potions.Signals;
using Underground.Items.Rocks;
using Underground.Items.Rocks.Signals;
using Underground.Items.Signals;
using Underground.Items.Traps;
using Underground.Items.Traps.Signals;
using Underground.Items.Coins;
using Underground.Items.Coins.Signals;

using UnityEngine;
using Zenject;

namespace Underground.Items.Installers
{
    public class ItemsInstaller : MonoInstaller<ItemsInstaller>
    {
        [SerializeField] private GameObject trapPrefab;
        [SerializeField] private GameObject rockPrefab;

        public override void InstallBindings()
        {
            Container.Bind<BearTrapSpawner>().AsSingle().NonLazy();
            Container.Bind<RockSpawner>().AsSingle().NonLazy();
            Container.Bind<RockController>().AsSingle().NonLazy();
            Container.BindFactory<BearTrap, BearTrap.Factory>().FromComponentInNewPrefab(trapPrefab);
            Container.BindFactory<Rock, Rock.Factory>().FromComponentInNewPrefab(rockPrefab);
            InstallSignals();
        }

        public void InstallSignals()
        {
            Container.DeclareSignal<UseItemSignal>();
            Container.DeclareSignal<ChooseItemSignal>();
            Container.DeclareSignal<TriggerBearTrapSignal>();

            Container.DeclareSignal<PickBearTrapSignal>();
            Container.DeclareSignal<PickHealthPotionSignal>();
            Container.DeclareSignal<PickArmorSignal>();
            Container.DeclareSignal<PickBootsSignal>();
            Container.DeclareSignal<PickRockSignal>();
            Container.DeclareSignal<PickUpCoinSignal>();
        }
    }
}