using System.Collections;
using System.Collections.Generic;
using Underground.Items.Traps.Signals;
using Underground.Units;
using UnityEngine;
using Zenject;

namespace Underground.Items.Traps
{
    public class BearTrap : MonoBehaviour
    {
        [SerializeField] private float yTrapOffset = 0.25f;

        private Animator animator;
        private SignalBus signalBus;


        [Inject]
        private void Construct(SignalBus signalBus, Animator animator)
        {
            this.signalBus = signalBus;
            this.animator = animator;
        }

        private void Start()
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - yTrapOffset, 0f);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponent<Unit>().TakeDamage(10);
                TriggerTrap();
            }
        }

        private IEnumerator OnTrigger()
        {
            animator.SetBool("triggered", true);
            signalBus.Fire<TriggerBearTrapSignal>();
            yield return new WaitForSeconds(0.5f);
            Destroy(this.gameObject);
        }

        private void TriggerTrap()
        {
            StartCoroutine(OnTrigger());
        }

        public class Factory : PlaceholderFactory<BearTrap> { }
    }
}