using System.Collections;
using System.Collections.Generic;
using Underground.Players;
using UnityEngine;

namespace Underground.Items.Traps
{
    public class BearTrapSpawner
    {
        private readonly BearTrap.Factory trapFactory;
        private readonly Player player;

        public BearTrapSpawner(BearTrap.Factory trapFactory, Player player)
        {
            this.trapFactory = trapFactory;
            this.player = player;
        }

        public void SpawnTrap()
        {
            trapFactory.Create().transform.position = player.transform.position;
        }
    }
}