using System.Collections;
using System.Collections.Generic;
using Underground.Items.Traps.Signals;
using UnityEngine;
using Zenject;

namespace Underground.Items.Traps
{
    public class BearTrapIcon : MonoBehaviour
    {
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                signalBus.Fire<PickBearTrapSignal>();
                Destroy(this.gameObject);
            }
        }
    }
}