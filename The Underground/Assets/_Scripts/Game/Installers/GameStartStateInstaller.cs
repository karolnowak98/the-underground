using Underground.Game.States;
using Underground.Game.States.Factories;
using Underground.UI.InGame.Healths;
using Zenject;


namespace Underground.Game.Installers
{
    public class GameStartStateInstaller : MonoInstaller<GameStartStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<GameStartStateFactory>().FromSubContainerResolve().ByMethod(InstallGameStartStateFactory).AsSingle();
        }

        private void InstallGameStartStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<GameStartStateFactory>().AsSingle();
            subContainer.BindFactory<GameStartState, GameStartState.Factory>().AsSingle();
        }
    }
}