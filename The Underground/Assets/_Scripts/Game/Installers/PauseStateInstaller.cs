using Underground.Game.States;
using Underground.Game.States.Factories;
using Underground.Game.States.Signals;
using Zenject;

namespace Underground.Game.Installers
{
    public class PauseStateInstaller : MonoInstaller<PauseStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<PauseStateFactory>().FromSubContainerResolve().ByMethod(InstallPauseStateFactory).AsSingle();
            InstallSignals();
        }

        private void InstallPauseStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<PauseStateFactory>().AsSingle();
            subContainer.BindFactory<PauseState, PauseState.Factory>().AsSingle();
        }

        private void InstallSignals()
        {
            Container.DeclareSignal<ChangeToInGameStateSignal>();
        }
    }
}