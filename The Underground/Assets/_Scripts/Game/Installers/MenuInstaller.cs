using Underground.UI.Menus.Signals;
using Zenject;

namespace Underground.Game.Installers
{
    public class MenuInstaller : MonoInstaller<MenuInstaller>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            InstallSignals();
        }

        public void InstallSignals()
        {
            Container.DeclareSignal<ClickEnterSignal>();
            Container.DeclareSignal<ClickEscapeSignal>();
            Container.DeclareSignal<MoveMenuSignal>();
        }
    }
}