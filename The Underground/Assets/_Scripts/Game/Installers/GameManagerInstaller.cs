using Underground.Game.Managers;
using Zenject;

namespace Underground.Game.Installers
{
    public class GameManagerInstaller : MonoInstaller<GameManagerInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameManager>().FromSubContainerResolve().ByMethod(InstallGameManager).AsSingle().NonLazy();
            InstallSignals();
        }

        private void InstallGameManager(DiContainer subContainer)
        {
            subContainer.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
        }

        private void InstallSignals()
        {
            SignalBusInstaller.Install(Container);
        }
    }
}