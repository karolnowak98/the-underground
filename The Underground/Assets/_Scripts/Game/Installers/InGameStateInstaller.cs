using Underground.Game.States;
using Underground.Game.States.Factories;
using Underground.Game.States.Signals;
using Zenject;

namespace Underground.Game.Installers
{
    public class InGameStateInstaller : MonoInstaller<InGameStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<InGameStateFactory>().FromSubContainerResolve().ByMethod(InstallInGameStateFactory).AsSingle();
            InstallSignals();
        }

        private void InstallInGameStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<InGameStateFactory>().AsSingle();
            subContainer.BindFactory<InGameState, InGameState.Factory>().AsSingle();
        }

        private void InstallSignals()
        {
            Container.DeclareSignal<ChangeToPauseStateSignal>();
        }
    }
}