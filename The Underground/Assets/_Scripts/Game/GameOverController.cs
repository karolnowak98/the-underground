using System.Collections;
using System.Collections.Generic;
using Underground.Players.Signals;
using Underground.SceneLoaders;
using UnityEngine;
using Zenject;

public class GameOverController : MonoBehaviour
{
    private SignalBus signalBus;
    private SceneLoader sceneLoader;

    [Inject]
    private void Construct(SignalBus signalBus, SceneLoader sceneLoader)
    {
        this.signalBus = signalBus;
        this.sceneLoader = sceneLoader;
    }

    private void OnEnable()
    {
        signalBus.Subscribe<PlayerDiedSignal>(LoadGameOverMenu);

    }

    private void OnDisable()
    {
        signalBus.Unsubscribe<PlayerDiedSignal>(LoadGameOverMenu);
    }

    private void LoadGameOverMenu()
    {
        StartCoroutine(LoadGameOver());
        
    }

    private IEnumerator LoadGameOver()
    {
        yield return new WaitForSeconds(3f);
        sceneLoader.LoadGameOverMenuScene();
    }
}
