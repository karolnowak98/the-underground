using UnityEngine;

namespace Underground.Game.States
{
    public abstract class GameState
    {
        public GameState()
        {
        }

        public virtual void Init()
        {
            Debug.Log(ToString() + " Init.");
        }

        public virtual void Tick()
        {
        }

        public virtual void FixedTick()
        {
        }

        public virtual void LateTick()
        {
        }

        public virtual void Deinit()
        {
            Debug.Log(ToString() + " Deinit.");
        }
    }
}