using Underground.Cameras;
using Underground.Game.Managers;
using Underground.Game.States.Factories;
using Underground.Game.States.Signals;
using Underground.Maps;
using Underground.Units;
using Underground.Players;
using UnityEngine;
using Zenject;
using Underground.Pause;
using Underground.Audio;

namespace Underground.Game.States
{
    public class InGameState : GameState
    {
        readonly PauseStateFactory pauseStateFactory;
        private GameManager gameManager;
        private SignalBus signalBus;
        private MapFacade mapFacade;
        private UnitsFacade unitsFacade;
        private CameraFollow cameraFollow;
        private FogOfWarManager fogOfWarManager;
        private Player player;
        private PauseManager pauseManager;
        private MenuMusicManager menuMusicManager;

        public InGameState(
            GameManager gameManager,
            SignalBus signalBus,
            MapFacade mapFacade,
            UnitsFacade unitsFacade,
            CameraFollow cameraFollow,
            FogOfWarManager fogOfWarManager,
            PauseStateFactory pauseStateFactory,
            Player player,
            PauseManager pauseManager)
        {
            this.gameManager = gameManager;
            this.signalBus = signalBus;
            this.mapFacade = mapFacade;
            this.unitsFacade = unitsFacade;
            this.cameraFollow = cameraFollow;
            this.fogOfWarManager = fogOfWarManager;
            this.pauseStateFactory = pauseStateFactory;
            this.player = player;
            this.pauseManager = pauseManager;
        }

        public override void Init()
        {
            base.Init();
            signalBus.Subscribe<ChangeToPauseStateSignal>(SetPauseState);
            menuMusicManager = MenuMusicManager.Instance;
            menuMusicManager?.MuteMusic();
        }

        public override void Tick()
        {
            base.Tick();
            player.Tick();
            unitsFacade.Tick();
            fogOfWarManager.Tick();
            pauseManager.Tick();
        }

        public override void FixedTick()
        {
            base.FixedTick();
            player.FixedTick();
            unitsFacade.FixedTick();
            cameraFollow.FixedTick();
        }

        public override void Deinit()
        {
            base.Deinit();
            signalBus.Unsubscribe<ChangeToPauseStateSignal>(SetPauseState);
            menuMusicManager?.UnmuteMusic();
        }

        private void SetPauseState()
        {
            gameManager.SetState(pauseStateFactory.CreateState());
        }

        public class Factory : PlaceholderFactory<InGameState>
        {
        }
    }
}