using Underground.Cameras;
using Underground.Game.Managers;
using Underground.Game.States.Factories;
using Underground.Maps;
using Underground.Units;
using Underground.Players;
using Zenject;
using UnityEngine;

namespace Underground.Game.States
{
    public class GameStartState : GameState
    {
        readonly InGameStateFactory inGameStateFactory;
        private GameManager gameManager;
        private MapFacade mapFacade;
        private UnitsFacade unitsFacade;
        private CameraFollow cameraFollow;
        private FogOfWarManager fogOfWarManager;
        private Player player;
        
        public GameStartState(
            GameManager gameManager,
            MapFacade mapFacade,
            UnitsFacade unitsFacade,
            CameraFollow cameraFollow,
            FogOfWarManager fogOfWarManager,
            Player player,
            InGameStateFactory inGameStateFactory)
        {
            this.gameManager = gameManager;
            this.mapFacade = mapFacade;
            this.unitsFacade = unitsFacade;
            this.cameraFollow = cameraFollow;
            this.fogOfWarManager = fogOfWarManager;
            this.player = player;
            this.inGameStateFactory = inGameStateFactory;
        }

        public override void Init()
        {
            base.Init();
            mapFacade.Init();
            fogOfWarManager.Init();
            unitsFacade.Init();
            cameraFollow.Init();
            player.Init();
            gameManager.SetState(inGameStateFactory.CreateState());
        }

        public class Factory : PlaceholderFactory<GameStartState>
        {
        }
    }
}
