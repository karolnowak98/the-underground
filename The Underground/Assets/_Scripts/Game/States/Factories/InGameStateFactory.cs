namespace Underground.Game.States.Factories
{
    public class InGameStateFactory
    {
        readonly InGameState.Factory inGameStateFactory;

        public InGameStateFactory(InGameState.Factory inGameStateFactory)
        {
            this.inGameStateFactory = inGameStateFactory;
        }

        public GameState CreateState()
        {
            InGameState inGameState = inGameStateFactory.Create();
            return inGameState;
        }
    }
}