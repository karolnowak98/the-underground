namespace Underground.Game.States.Factories
{
    public class GameStartStateFactory
    {
        readonly GameStartState.Factory gameStartStateFactory;

        public GameStartStateFactory(GameStartState.Factory gameStartStateFactory)
        {
            this.gameStartStateFactory = gameStartStateFactory;
        }

        public GameState CreateState()
        {
            GameStartState gameStartState = gameStartStateFactory.Create();
            return gameStartState;
        }
    }
}