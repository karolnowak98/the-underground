namespace Underground.Game.States.Factories
{
    public class PauseStateFactory
    {
        readonly PauseState.Factory pauseStateFactory;

        public PauseStateFactory(PauseState.Factory pauseStateFactory)
        {
            this.pauseStateFactory = pauseStateFactory;
        }

        public GameState CreateState()
        {
            PauseState pauseState = pauseStateFactory.Create();
            return pauseState;
        }
    }
}