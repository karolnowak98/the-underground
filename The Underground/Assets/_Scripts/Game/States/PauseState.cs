using Zenject;
using Underground.Game.States.Signals;
using Underground.Game.Managers;
using Underground.Game.States.Factories;
using Underground.Units;
using Underground.Pause;
using Underground.Players;
using Underground.Audio;

namespace Underground.Game.States
{
    public class PauseState : GameState
    {
        readonly InGameStateFactory inGameStateFactory;
        private SignalBus signalBus;
        private GameManager gameManager;
        private UnitsFacade unitsFacade;
        private PauseManager pauseManager;
        private Player player;
        private MenuMusicManager menuMusicManager;

        public PauseState(
            SignalBus signalBus,
            GameManager gameManager,
            InGameStateFactory inGameStateFactory,
            UnitsFacade unitsFacade,
            PauseManager pauseManager,
            Player player)
        {
            this.signalBus = signalBus;
            this.gameManager = gameManager;
            this.inGameStateFactory = inGameStateFactory;
            this.unitsFacade = unitsFacade;
            this.pauseManager = pauseManager;
            this.player = player;
        }

        public override void Init()
        {
            base.Init();
            signalBus.Subscribe<ChangeToInGameStateSignal>(SetInGameState);
            unitsFacade.SetActiveAnimators(false);
            player.SetActiveAnimator(false);
            menuMusicManager = MenuMusicManager.Instance;
            menuMusicManager?.UnmuteMusic();
            player.SetPause(true);
        }

        public override void Tick()
        {
            base.Tick();
            pauseManager.Tick();
        }

        private void SetInGameState()
        {
            gameManager.SetState(inGameStateFactory.CreateState());
        }

        public override void Deinit()
        {
            base.Deinit();
            unitsFacade.SetActiveAnimators(true);
            player.SetActiveAnimator(true);
            signalBus.Unsubscribe<ChangeToInGameStateSignal>(SetInGameState);
            menuMusicManager?.UnmuteMusic();
            player.SetPause(false);
        }

        public class Factory : PlaceholderFactory<PauseState>
        {
        }
    }
}
