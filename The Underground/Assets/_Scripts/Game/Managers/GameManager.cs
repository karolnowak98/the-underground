using System;
using Underground.Game.States;
using Underground.Game.States.Factories;
using Zenject;

namespace Underground.Game.Managers
{
    public class GameManager : IInitializable, ITickable, IFixedTickable, ILateTickable, IDisposable
    {
        private GameState currentState;

        [Inject]
        public void Construct(GameStartStateFactory gameStartStateFactory)
        {
            currentState = gameStartStateFactory.CreateState();
        }

        public void SetState(GameState nextState)
        {
            currentState.Deinit();
            currentState = nextState;
            currentState.Init();
        }

        public void Initialize()
        {
            currentState.Init();
        }

        public void Tick()
        {
            currentState.Tick();
        }

        public void FixedTick()
        {
            currentState.FixedTick();
        }

        public void LateTick()
        {
            currentState.LateTick();
        }

        public void Dispose()
        {
            currentState.Deinit();
        }
    }
}