using System.Collections;
using System.Collections.Generic;
using Underground.Players.Signals;
using UnityEngine;
using Zenject;

public class GameOverInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.DeclareSignal<PlayerDiedSignal>();
    }
}
