using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Underground.Audio
{
    [System.Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
        [Range(0f, 1f)]
        public float volume = 0.8f; 
        [Range(0.1f, 3f)]
        public float pitch = 1f;

        [Range(0f, 0.2f)]
        public float randomness = 0f;
    }
}

