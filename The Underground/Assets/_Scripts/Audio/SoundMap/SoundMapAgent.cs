using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Underground.Maps;
using Zenject;
using System;

namespace Underground.Audio.SoundMaps
{
    public class SoundMapAgent : MonoBehaviour
    {
        private MapFacade _mapFacade;
        private int _numberOfRays = 64;
        private Vector2 _lastPositionOfSound;
        private int _lastLoudnessOfSound;
        private bool _soundMapCleared = true;

        [Inject]
        private void Construct(MapFacade mapFacade)
        {
            _mapFacade = mapFacade;
        }

        // Start is called before the first frame update
        void Start()
        {
            _lastPositionOfSound = new Vector2(-1, -1);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void FixedUpdate()
        {
            if (!_soundMapCleared)
            {
                for (int i = (int)_lastPositionOfSound.x - (_lastLoudnessOfSound+1); i <= (int)_lastPositionOfSound.x + (_lastLoudnessOfSound + 1); i++)
                {
                    for (int j = (int)_lastPositionOfSound.y - (_lastLoudnessOfSound + 1); j <= (int)_lastPositionOfSound.y + (_lastLoudnessOfSound + 1); j++)
                    {
                        Node node = _mapFacade.GetNodeFromPos(new Vector3(i, j, 0));
                        if (node != null)
                            node.SoundLevel = 0;
                    }
                }
                _soundMapCleared = true;
            }

        }

        public void AddNoise(Vector3 pos, float loudness)
        {
            _soundMapCleared = false;
            _lastPositionOfSound = new Vector2(pos.x, pos.y);
            _lastLoudnessOfSound = (int)loudness;
            List<Vector2> rays = GetRays(pos, loudness);

            foreach (Vector2 ray in rays)
            {
                for (int i = Mathf.FloorToInt(ray.magnitude); i >= 0; i--)
                {
                    AddNoiseToSoundMap(pos, ray.normalized, i, loudness);
                }
            }
        }

        private void AddNoiseToSoundMap(Vector3 pos, Vector2 rayDirection, int rayDistance, float loundess)
        {
            Vector2 position = new Vector2( pos.x, pos.y) + (rayDirection * rayDistance);
            Node node = _mapFacade.GetNodeFromPos(position);

            if (node != null && node.SoundLevel == 0 && node.Flyable)
            {
                node.SoundLevel = (int)loundess - rayDistance;
            }
        }

        private List<Vector2> GetRays(Vector3 pos, float loudness)
        {
            List<Vector2> rays = new List<Vector2>();
            if (loudness > 6)
                _numberOfRays = 64;
            else if (loudness > 4)
                _numberOfRays = 32;
            else if (loudness > 0)
                _numberOfRays = 16;

            for (int i = 0; i < _numberOfRays; i++)
            {
                Vector3 direction = (Quaternion.Euler(0, 0, (360 / _numberOfRays) * i) * Vector3.up).normalized;
                RaycastHit2D[] hits = Physics2D.RaycastAll(pos, direction, loudness);

                float distanceToClosestSoundBlocker = loudness;

                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.transform.CompareTag("Wall"))
                    {
                        float distance = Vector3.Distance(hit.point, pos);
                        if (distance < distanceToClosestSoundBlocker)
                            distanceToClosestSoundBlocker = distance;
                    }
                }

                rays.Add(direction * distanceToClosestSoundBlocker);
            }
            return rays;
        }

        public bool NodeContainSound(Vector3 pos)
        {
            Node node = _mapFacade.GetNodeFromPos(pos);
            if (node.SoundLevel > 0)
                return true;
            else
                return false;
        }
    }
}

