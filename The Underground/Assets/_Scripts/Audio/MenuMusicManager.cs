using UnityEngine;
using Zenject;

namespace Underground.Audio
{
    public class MenuMusicManager : MonoBehaviour
    {
        private AudioSource audioSource;

        public static MenuMusicManager Instance;

        [Inject]
        private void Construct(AudioSource audioSource)
        {
            this.audioSource = audioSource;
        }

        private void Start()
        {
            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else
                Destroy(gameObject);
        }

        public void MuteMusic()
        {
            audioSource.mute = true;
        }

        public void UnmuteMusic()
        {
            audioSource.mute = false;
        }
    }
}
