using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Underground.Audio
{
    public class AudioManager: MonoBehaviour
    {
        public List<Sound> sounds;
        private AudioSource _audioSource;
        private string _currentClip;

        public void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
        }

        public void Play(string name, float soundLevel = -100)
        {
            //if(_audioSource == null)
            //    _audioSource = GetComponent<AudioSource>();

            if (_audioSource.isPlaying && name != _currentClip)
            {
                _audioSource.Stop();
            }
            
            if(!_audioSource.isPlaying)
            {
                foreach (Sound s in sounds)
                {
                    
                    if (name == s.name)
                    {
                        _audioSource.clip = s.clip;
                        _currentClip = name;
                        if(s.randomness > 0)
                        {
                            _audioSource.volume = s.volume + Random.Range(-s.randomness * s.volume, s.randomness * s.volume);
                            _audioSource.pitch = s.pitch + Random.Range(-s.randomness, s.randomness);
                        }
                        else
                        {
                            _audioSource.volume = s.volume;
                            _audioSource.pitch = s.pitch;
                        }

                        if (soundLevel > 0 && soundLevel <= 1)
                        {
                            _audioSource.volume *= soundLevel;
                        }
                        _audioSource.Play();
                    }
                }
            }
        }

        public void StopPlaying()
        {
            _audioSource.Stop();
        }

        public void PlayClipAtPoint(Vector3 pos, string name)
        {
            if (!_audioSource.isPlaying)
            {
                foreach (Sound s in sounds)
                {
                    Debug.Log(sounds.Count + s.name + " " + name);
                    if (name == s.name)
                    {
                        float volume;
                        
                        if (s.randomness > 0)
                            volume = s.volume + Random.Range(-s.randomness * s.volume, s.randomness * s.volume);
                        else
                            volume = s.volume;

                        AudioSource.PlayClipAtPoint(s.clip, pos, volume);
                    }
                }
            }
        }
    }
}

