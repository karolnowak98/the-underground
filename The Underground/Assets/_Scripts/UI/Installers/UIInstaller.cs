using Underground.UI.InGame.Healths;
using Underground.UI.InGame.Items;
using Underground.UI.InGame.Coins;
using Zenject;

public class UIInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<PlayerHealth>().AsSingle();
        Container.Bind<ItemsNumbers>().AsSingle();
        Container.Bind<PlayerCoins>().AsSingle();
    }
}
