namespace Underground.UI.InGame.Healths
{
    public class PlayerHealth
    {
        public delegate void HealthChangedHandler();
        public event HealthChangedHandler HealthChanged;
        public event HealthChangedHandler HealthDecreased;
        public event HealthChangedHandler HealthAtZero;

        private float health = 3;

        public float Health
        {
            get => health;
            set
            {
                float previousValue = health;
                if (previousValue != value)
                {
                    health = value;

                    if (previousValue > value)
                        HealthDecreased?.Invoke();

                    if (health == 0)
                        HealthAtZero?.Invoke();
                    
                    HealthChanged?.Invoke();
                }
            }
        }
    }
}