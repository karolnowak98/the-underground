﻿using Underground.Items.Armors.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Underground.UI.InGame.Healths
{
    public class HealthBarController : MonoBehaviour
    {
        public Transform heartsParent;
        public GameObject heartContainerPrefab;

        [SerializeField] private int maxHealth;
        [SerializeField] private int maxTotalHealth;

        private GameObject[] heartContainers;
        private Image[] heartFills;
        private PlayerHealth playerHealth;

        [Inject]
        private void Construct(PlayerHealth playerHealth)
        {
            this.playerHealth = playerHealth;
        }

        private void Start()
        {
            heartContainers = new GameObject[maxTotalHealth];
            heartFills = new Image[maxTotalHealth];

            playerHealth.HealthChanged += UpdateHeartsHUD;
            InstantiateHeartContainers();
            UpdateHeartsHUD();
        }

        private void UpdateHeartsHUD()
        {
            SetHeartContainers();
            SetFilledHearts();
        }

        private void SetHeartContainers()
        {
            for (int i = 0; i < heartContainers.Length; i++)
            {
                if (i < maxHealth)
                {
                    heartContainers[i].SetActive(true);
                }
                else
                {
                    heartContainers[i].SetActive(false);
                }
            }
        }

        private void SetFilledHearts()
        {
            for (int i = 0; i < heartFills.Length; i++)
            {
                if (i < playerHealth.Health)
                {
                    heartFills[i].fillAmount = 1;
                }
                else
                {
                    heartFills[i].fillAmount = 0;
                }
            }

            if (playerHealth.Health % 1 != 0)
            {
                int lastPos = Mathf.FloorToInt(playerHealth.Health);
                heartFills[lastPos].fillAmount = playerHealth.Health % 1;
            }
        }

        private void InstantiateHeartContainers()
        {
            for (int i = 0; i < maxTotalHealth; i++)
            {
                GameObject temp = Instantiate(heartContainerPrefab);
                temp.transform.SetParent(heartsParent, false);
                heartContainers[i] = temp;
                heartFills[i] = temp.transform.Find("HeartFill").GetComponent<Image>();
            }
        }

        public void Heal(float health)
        {
            playerHealth.Health = Mathf.Clamp(playerHealth.Health + health, 0, maxHealth);
        }

        public void TakeDamage(float dmg)
        {
            playerHealth.Health = Mathf.Clamp(playerHealth.Health - dmg, 0, maxHealth);
        }

        public void AddHealth(int heartsNumber)
        {
            if (maxHealth < maxTotalHealth)
            {
                maxHealth += heartsNumber;
                Heal(heartsNumber);
            }
        }

    }
}