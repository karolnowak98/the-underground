using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Underground.UI.InGame.Coins
{
    public class CoinsSignalInstaller : MonoInstaller<CoinsSignalInstaller>
    {
        public override void InstallBindings()
        {
            InstallSignals();
        }

        public void InstallSignals()
        {
            Container.DeclareSignal<CoinsAmountChanged>();
        }
    }

}

