using System.Collections;
using System.Collections.Generic;
using Underground.Items.Coins.Signals;
using Underground.UI.InGame.Coins;
using TMPro;
using UnityEngine;
using Zenject;
using System;

namespace Underground.UI.InGame.Coins
{
    public class CoinsController : MonoBehaviour
    {
        [SerializeField] TMP_Text coinsAmountTMP;
        private SignalBus signalBus;
        private PlayerCoins playerCoins;

        [Inject]
        private void Constuct(SignalBus signalBus, PlayerCoins playerCoins)
        {
            this.signalBus = signalBus;
            this.playerCoins = playerCoins;
        }

        private void Awake()
        {
            signalBus.Subscribe<CoinsAmountChanged>(UpdateCoinsAmount);
        }

        private void UpdateCoinsAmount()
        {
            coinsAmountTMP.text = playerCoins.Coins.ToString();
        }
    }
}

