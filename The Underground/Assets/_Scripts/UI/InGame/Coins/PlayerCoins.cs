using Underground.Items.Coins.Signals;
using Zenject;

namespace Underground.UI.InGame.Coins
{
    public class PlayerCoins
    {
        private float coins = 0;
        private SignalBus signalBus;
        public PlayerCoins(SignalBus signalBus)
        {
            this.signalBus = signalBus;
            signalBus.Subscribe<PickUpCoinSignal>(CoinPickedUp);
        }
        public float Coins
        {
            get => coins;
            set
            {
                float previousValue = coins;
                if (previousValue != value)
                {
                    coins = value;
                    signalBus.Fire<CoinsAmountChanged>();
                }
            }
        }

        private void CoinPickedUp()
        {
            Coins += 1;
        }
    }
}