namespace Underground.UI.InGame.Items
{
    public class ItemsNumbers
    {
        public delegate void BearTrapsNumberChangedHandler();
        public event BearTrapsNumberChangedHandler BearTrapsNumberChanged;

        public delegate void RocksNumberChangedHandler();
        public event RocksNumberChangedHandler RocksNumberChanged;

        private int bearTrapsNumber = 1;
        private int rocksNumber = 1;


        public int RocksNumber
        {
            get => rocksNumber;
            set
            {
                int previousValue = rocksNumber;
                if (previousValue != value)
                {
                    rocksNumber = value;
                    RocksNumberChanged?.Invoke();
                }
            }
        }
        public int BearTrapsNumber
        {
            get => bearTrapsNumber;
            set
            {
                int previousValue = bearTrapsNumber;
                if (previousValue != value)
                {
                    bearTrapsNumber = value;
                    BearTrapsNumberChanged?.Invoke();
                }
            }
        }
    }
}