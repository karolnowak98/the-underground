using System.Collections;
using System.Collections.Generic;
using TMPro;
using Underground.Audio;
using Underground.Items.Rocks;
using Underground.Items.Signals;
using Underground.Items.Traps;
using Underground.Items.Traps.Signals;
using Underground.Players;
using UnityEngine;
using Zenject;

namespace Underground.UI.InGame.Items
{
    public class ItemsController : MonoBehaviour
    {
        [SerializeField] TMP_Text trapsNumberTMP;
        [SerializeField] TMP_Text rocksNumberTMP;
        [SerializeField] GameObject trapsUI;
        [SerializeField] GameObject rocksUI;
        [SerializeField] int trapsNumber;
        [SerializeField] int rocksNumber;
        [SerializeField] int maxItemIndex;
        
        public int itemIndex = 0;
        private SignalBus signalBus;
        private BearTrapSpawner trapSpawner;
        private ItemsNumbers itemsNumbers;
        private AudioManager audioManager;
        private RockController rocksController;
        private bool isTrapSpawning = false;

        [Inject]
        private void Construct(SignalBus signalBus, BearTrapSpawner trapSpawner, ItemsNumbers itemsNumbers, RockController rocksController)
        {
            this.signalBus = signalBus;
            this.trapSpawner = trapSpawner;
            this.itemsNumbers = itemsNumbers;
            this.rocksController = rocksController;
        }

        private void Start()
        {
            audioManager = GetComponent<AudioManager>();
            itemsNumbers.RocksNumber = rocksNumber;
            itemsNumbers.BearTrapsNumber = trapsNumber;
            UpdateRocksTMP();
            rocksUI.SetActive(false);
        }

        private void OnEnable()
        {
            signalBus.Subscribe<UseItemSignal>(UseItem);
            signalBus.Subscribe<TriggerBearTrapSignal>(TriggerTrap);
            signalBus.Subscribe<ChooseItemSignal>(ChooseItem);

            itemsNumbers.BearTrapsNumberChanged += UpdateTrapsTMP;
            itemsNumbers.RocksNumberChanged += UpdateRocksTMP;
        }

        private void OnDisable()
        {
            signalBus.Unsubscribe<UseItemSignal>(UseItem);
            signalBus.Unsubscribe<TriggerBearTrapSignal>(TriggerTrap);
            signalBus.Unsubscribe<ChooseItemSignal>(ChooseItem);

            itemsNumbers.BearTrapsNumberChanged -= UpdateTrapsTMP;
            itemsNumbers.RocksNumberChanged += UpdateRocksTMP;
        }

        private void UseItem()
        {
            if(itemIndex == 0)
            {
                SetTrap();
            }
            else if (itemIndex == 1)
            {
                ThrowRock();
            }
        }

        private void SetTrap()
        {
            if (itemsNumbers.BearTrapsNumber > 0)
            {
                if (!isTrapSpawning)
                {
                    StartCoroutine(TrapSpawnDelay());
                }
            }
        }

        private IEnumerator TrapSpawnDelay()
        {
            
            isTrapSpawning = true;
            trapSpawner.SpawnTrap();
            audioManager.Play("SetTrap");
            itemsNumbers.BearTrapsNumber--;
            yield return new WaitForSeconds(1.5f);
            isTrapSpawning = false;
            audioManager.StopPlaying();
        }

        private void ThrowRock()
        {
            if(itemsNumbers.RocksNumber > 0)
            {
                rocksController.Throw();
                itemsNumbers.RocksNumber--;
            }
        }
        
        private void TriggerTrap()
        {
            audioManager.Play("Trigger");
        }

        public void AddTrap(int number)
        {
            itemsNumbers.BearTrapsNumber += number;
        }

        public void AddRock(int number)
        {
            itemsNumbers.RocksNumber += number;
        }

        private void UpdateTrapsTMP()
        {
            trapsNumberTMP.SetText($"x{itemsNumbers.BearTrapsNumber}");
        }

        private void UpdateRocksTMP()
        {
            rocksNumberTMP.SetText($"x{itemsNumbers.RocksNumber}");
        }

        private void ChooseItem()
        {
            if (itemIndex < maxItemIndex)
            {
                itemIndex++;
                trapsUI.SetActive(false);
                rocksUI.SetActive(true);
            }
            else
            {
                itemIndex = 0;
                trapsUI.SetActive(true);
                rocksUI.SetActive(false);
            }
        }
    }
}