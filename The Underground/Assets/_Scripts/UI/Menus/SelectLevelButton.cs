using System.Collections;
using System.Collections.Generic;
using Underground.Maps.Settings;
using UnityEngine;
using Zenject;
using UnityEngine.UI;

public class SelectLevelButton : MonoBehaviour
{
    private MapSettings mapSettings;
    private Button button;
    [SerializeField] private int levelId;

    [Inject]
    private void Construct(
        MapSettings mapSettings,
        Button button)
    {
        this.mapSettings = mapSettings;
        this.button = button;
    }

    private void Start()
    {
        if (levelId == 1)
            button.interactable = true;
        else

        button.interactable = (PlayerPrefs.GetInt("Level" + levelId + "Completed") == 1) ? true : false;
    }
}
