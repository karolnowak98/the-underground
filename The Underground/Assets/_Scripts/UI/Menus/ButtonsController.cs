using Underground.Audio;
using Underground.SceneLoaders;
using Underground.UI.Menus.Signals;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Underground.UI.Menus
{
    public class ButtonsController : MonoBehaviour
    {
        public int btnIndex;

        [SerializeField] int maxIndex;
        [SerializeField] int subMenuIndex;

        private SignalBus signalBus;
        private AudioManager audioManager;
        private Vector2 moveAxis;
        private SceneLoader sceneLoader;
        private int soundNumber;
        [Inject(Optional = true, Id = "ButtonLevel1")] Button buttonLevel1;
        [Inject(Optional = true, Id = "ButtonLevel2")] Button buttonLevel2;
        [Inject(Optional = true, Id = "ButtonLevel3")] Button buttonLevel3;

        [Inject]
        private void Construct(SignalBus signalBus,
            SceneLoader sceneLoader)
        {
            this.signalBus = signalBus;
            this.sceneLoader = sceneLoader;
        }

        private void Start()
        {
            audioManager = GetComponent<AudioManager>();
        }

        private void HandleMoving()
        {
            if (moveAxis.y == 1)
            {
                if (btnIndex > 0)
                {
                    soundNumber = Random.Range(1,3);
                    audioManager.Play($"Select{soundNumber}");
                    btnIndex--;
                    signalBus.Fire<MoveMenuSignal>();
                }
            }

            if (moveAxis.y == -1)
            {
                if (btnIndex < maxIndex)
                {
                    soundNumber = Random.Range(1, 3);
                    audioManager.Play($"Select{soundNumber}");
                    btnIndex++;
                    signalBus.Fire<MoveMenuSignal>();
                }
            }
        }

        private void ChooseRightActionForEnter()
        {
            switch (subMenuIndex)
            {
                case 0:
                    switch (btnIndex)
                    {
                        case 0:
                            ChangeToSelectLevelState();
                            break;
                        case 1:
                            ChangeToControlsState();
                            break;
                        case 2:
                            ChangeToCreditsState();
                            break;
                        case 3:
                            Application.Quit();
                            break;
                    }
                    break;
                case 1:
                    switch (btnIndex)
                    {
                        case 0:
                            ChangeToInGameState("Level1");
                            break;
                        case 1:
                            if (PlayerPrefs.GetInt("Level" + 2 + "Completed") == 1)
                            {
                                ChangeToInGameState("Level2");
                            }
                            break;
                        case 2:
                            if (PlayerPrefs.GetInt("Level" + 3 + "Completed") == 1)
                            {
                                ChangeToInGameState("Level3");
                            }
                            break;
                    }
                    break;
                case 2:
                    switch (btnIndex)
                    {
                        case 0:
                            ChangeToSelectLevelState();
                            break;
                        case 1:
                            ChangeToMainMenuState();
                            break;
                    }
                    break;
                case 3:
                case 4:
                    ChangeToMainMenuState();
                    break;
            }
        }

        private void ChooseRightActionForEscape()
        {
            switch (subMenuIndex)
            {
                case 0:
                    break;
                default:
                    ChangeToMainMenuState();
                    break;
            }
        }

        public void Move(InputAction.CallbackContext ctx)
        {
            moveAxis = ctx.ReadValue<Vector2>();
            if (ctx.performed)
            {
                HandleMoving();
            }
        }

        public void Enter(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                signalBus.Fire<ClickEnterSignal>();
                ChooseRightActionForEnter();
            }
        }

        public void Escape(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                signalBus.Fire<ClickEscapeSignal>();
                ChooseRightActionForEscape();
            }
        }

        public void ChangeToControlsState()
        {
            sceneLoader.LoadControlsMenuScene();
        }

        public void ChangeToCreditsState()
        {
            sceneLoader.LoadCreditsMenuScene();
        }

        public void ChangeToMainMenuState()
        {
            sceneLoader.LoadMainMenuScene();
        }

        public void ChangeToSelectLevelState()
        {
            sceneLoader.LoadSelectLevelScene();
        }

        public void ChangeToInGameState(string sceneName)
        {
            sceneLoader.LoadSceneByName(sceneName);
        }
    }
}