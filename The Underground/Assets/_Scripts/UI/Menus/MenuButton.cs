using System.Collections;
using System.Collections.Generic;
using Underground.UI.Menus.Signals;
using UnityEngine;
using Zenject;

namespace Underground.UI.Menus
{
    public class MenuButton : MonoBehaviour
    {
        [SerializeField] int index;
        [SerializeField] Animator animator;
        [SerializeField] ButtonsController buttonController;

        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
            
        }

        private void Start()
        {
            if(index == 0)
            {
                animator.SetBool("selected", true);
            }
        }

        private void OnEnable()
        {
            signalBus.Subscribe<ClickEnterSignal>(HandleEnter);
            signalBus.Subscribe<ClickEscapeSignal>(HandleEscape);
            signalBus.Subscribe<MoveMenuSignal>(HandleMoving);
        }

        private void OnDisable()
        {
            signalBus.Unsubscribe<ClickEnterSignal>(HandleEnter);
            signalBus.Unsubscribe<ClickEscapeSignal>(HandleEscape);
            signalBus.Unsubscribe<MoveMenuSignal>(HandleMoving);
        }

        private void HandleEnter()
        {
            if (buttonController.btnIndex == index)
            {
                animator.SetBool("pressed", true);
            }
        }

        private void HandleEscape()
        {
            if (buttonController.btnIndex == index)
            {
                animator.SetBool("pressed", false);
                animator.SetBool("selected", false);
            }
        }

        private void HandleMoving()
        {
            if (buttonController.btnIndex == index)
            {
                animator.SetBool("selected", true);
            }
            else
            {
                animator.SetBool("selected", false);
            }
        }
    }
}