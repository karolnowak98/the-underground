using Underground.Units.States.Factories;
using Zenject;

namespace Underground.Units.States.Installers
{
    public class FollowTargetStateInstaller : MonoInstaller<FollowTargetStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<FollowTargetStateFactory>().FromSubContainerResolve().ByMethod(InstallFollowTargetStateFactory).AsSingle();
        }

        private void InstallFollowTargetStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<FollowTargetStateFactory>().AsSingle();
            subContainer.BindFactory<FollowTargetState, FollowTargetState.Factory>().AsSingle();
        }
    }
}
