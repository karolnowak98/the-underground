using Underground.Units.States.Factories;
using Zenject;

namespace Underground.Units.States.Installers
{
    public class AttackTargetStateInstaller : MonoInstaller<AttackTargetStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<AttackTargetStateFactory>().FromSubContainerResolve().ByMethod(InstallAttackTargetStateFactory).AsSingle();
        }

        private void InstallAttackTargetStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<AttackTargetStateFactory>().AsSingle();
            subContainer.BindFactory<AttackTargetState, AttackTargetState.Factory>().AsSingle();
        }
    }
}
