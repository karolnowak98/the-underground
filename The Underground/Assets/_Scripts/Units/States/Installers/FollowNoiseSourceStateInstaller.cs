using Underground.Units.States.Factories;
using Zenject;

namespace Underground.Units.States.Installers
{
    public class FollowNoiseSourceStateInstaller : MonoInstaller<FollowNoiseSourceStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<FollowNoiseSourceStateFactory>().FromSubContainerResolve().ByMethod(InstallFollowNoiseSourceStateFactory).AsSingle();
        }

        private void InstallFollowNoiseSourceStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<FollowNoiseSourceStateFactory>().AsSingle();
            subContainer.BindFactory<FollowNoiseSourceState, FollowNoiseSourceState.Factory>().AsSingle();
        }
    }
}