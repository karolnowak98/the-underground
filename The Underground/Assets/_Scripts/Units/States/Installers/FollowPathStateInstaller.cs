using Underground.Units.States.Factories;
using Zenject;

namespace Underground.Units.States.Installers
{
    public class FollowPathStateInstaller : MonoInstaller<FollowPathStateInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<FollowPathStateFactory>().FromSubContainerResolve().ByMethod(InstallFollowPathStateFactory).AsSingle();
        }

        private void InstallFollowPathStateFactory(DiContainer subContainer)
        {
            subContainer.Bind<FollowPathStateFactory>().AsSingle();
            subContainer.BindFactory<FollowPathState, FollowPathState.Factory>().AsSingle();
        }
    }
}
