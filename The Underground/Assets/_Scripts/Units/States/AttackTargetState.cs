using Underground.Units.Settings;
using UnityEngine;
using Zenject;
using Underground.UI.InGame.Healths;
using Underground.Units.Enemies;
using Underground.Audio;

namespace Underground.Units.States
{
    public class AttackTargetState : UnitState
    {
        private BasicEnemy basicEnemy;
        private Animator animator;
        private UnitSettings unitSettings;
        private Transform transform;
        private HealthBarController healthBarController;
        private AudioManager audioManager;
        private AudioSource audioSource;

        public AttackTargetState(
            BasicEnemy basicEnemy,
            Animator animator,
            UnitSettings unitSettings,
            Transform transform,
            HealthBarController healthBarController,
            AudioManager audioManager,
            AudioSource audioSource)
        {
            this.basicEnemy = basicEnemy;
            this.animator = animator;
            this.unitSettings = unitSettings;
            this.transform = transform;
            this.healthBarController = healthBarController;
            this.audioManager = audioManager;
            this.audioSource = audioSource;
        }

        public override void Init()
        {
            animator.SetBool("IsWalking", false);
            IsBusy = true;
            audioSource.loop = false;
        }

        public override void Tick()
        {
            if(basicEnemy.CooldownToNextAttack > 0)
            {
                if(basicEnemy.CooldownToNextAttack + 0.3f <= unitSettings.AttackCooldown && IsBusy)
                {
                    animator.SetBool("IsAttacking", false);
                    IsBusy = false;
                }

                return;
            }

            PerformAttack();
        }

        public override void Deinit()
        {
            animator.SetBool("IsAttacking", false);
            audioManager.StopPlaying();
            audioSource.loop = true;
        }

        private void PerformAttack()
        {
            audioManager.StopPlaying();
            animator.SetBool("IsAttacking", true);
            healthBarController.TakeDamage(1);
            basicEnemy.CooldownToNextAttack = unitSettings.AttackCooldown;
            IsBusy = true;
            audioManager.Play("Attack");
        }

        public class Factory : PlaceholderFactory<AttackTargetState>
        {
        }
    }
}
