namespace Underground.Units.States
{
    public abstract class UnitState
    {
        public abstract void Init();
        public abstract void Tick();
        public abstract void Deinit();
        public bool IsBusy { get; set; }
    }
}
