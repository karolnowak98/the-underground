using System.Collections.Generic;
using Underground.Audio;
using Underground.Maps;
using Underground.Pathfinding;
using Underground.Players;
using Underground.Units.Settings;
using Underground.Units.States.Factories;
using UnityEngine;
using Zenject;

namespace Underground.Units.States
{
    public class FollowTargetState : UnitState
    {
        private AStarPathfinding aStarPathfinding;
        private MapFacade mapFacade;
        private List<Node> path;
        private Transform transform;
        private UnitSettings unitSettings;
        private Animator animator;
        private Node lastTargetNode;
        private Node targetNode;
        private Unit unit;
        private AudioManager audioManager;
        private float timeToStopPlayingDetectSound;
        private AudioSource audioSource;
        private Player player;

        public FollowTargetState(
            AStarPathfinding aStarPathfinding,
            MapFacade mapFacade,
            Transform transform,
            Animator animator,
            Unit unit,
            UnitSettings unitSettings,
            AudioManager audioManager,
            AudioSource audioSource,
            Player player)
        {
            this.aStarPathfinding = aStarPathfinding;
            this.mapFacade = mapFacade;
            this.transform = transform;
            this.animator = animator;
            this.unit = unit;
            this.unitSettings = unitSettings;
            this.audioManager = audioManager;
            this.audioSource = audioSource;
            this.player = player;
        }

        public override void Init()
        {
            lastTargetNode = null;
            animator.SetBool("IsWalking", true);
            IsBusy = true;
            audioManager.Play("Detect");
            audioSource.loop = false;
            timeToStopPlayingDetectSound = 1f;
        }

        public override void Tick()
        {
            PlaySound();
            UpdateRotation();
            UpdatePathToTarget();
            if (!PathIsEmpty())
                Move();
            else
                IsBusy = false;
        }

        public override void Deinit()
        {
            animator.SetBool("IsWalking", false);
            audioManager.StopPlaying();
            audioSource.loop = true;
        }

        private void PlaySound()
        {
            if (timeToStopPlayingDetectSound > 0)
            {
                timeToStopPlayingDetectSound -= Time.deltaTime;
                if(timeToStopPlayingDetectSound <= 0)
                {
                    audioManager.StopPlaying();
                    audioManager.Play("Moving");
                    audioSource.loop = true;
                }
            }
        }

        private void UpdatePathToTarget()
        {
            if (unit.Target == null || player.IsInvisible == true)
                return;

            targetNode = mapFacade.GetNodeFromPos(unit.Target.transform.position);
            if (targetNode != lastTargetNode || PathIsEmpty()) 
            {
                if((targetNode.Walkable && unitSettings.CanFly == false) || (targetNode.Flyable && unitSettings.CanFly))
                    path = aStarPathfinding.FindPath(mapFacade.GetNodeFromPos(transform.position), targetNode, unitSettings.CanFly);
                lastTargetNode = targetNode;
            }
        }

        private void Move()
        {
            if (Vector3.Distance(transform.position, path[0].CenterBottomPos) < 0.1f)
                path.RemoveAt(0);
            else
                transform.position = Vector2.MoveTowards(transform.position, path[0].CenterBottomPos + Vector3.up * 0.02f, unitSettings.RunSpeed * Time.deltaTime);
        }

        private void UpdateRotation()
        {
            if (PathIsEmpty())
                return;

            float yRotation = (transform.position.x < path[0].CenterBottomPos.x) ? 0 : 180;
            transform.rotation = Quaternion.Euler(0, yRotation, 0);
        }

        private bool PathIsEmpty()
        {
            if (path == null || path.Count == 0)
                return true;

            return false;
        }

        public class Factory : PlaceholderFactory<FollowTargetState>
        {
        }
    }
}
