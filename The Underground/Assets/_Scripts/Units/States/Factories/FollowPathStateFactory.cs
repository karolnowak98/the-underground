namespace Underground.Units.States.Factories
{
    public class FollowPathStateFactory
    {
        readonly FollowPathState.Factory followPathStateFactory;

        public FollowPathStateFactory(FollowPathState.Factory followPathStateFactory)
        {
            this.followPathStateFactory = followPathStateFactory;
        }

        public FollowPathState CreateState()
        {
            FollowPathState followPathState = followPathStateFactory.Create();
            return followPathState;
        }
    }
}
