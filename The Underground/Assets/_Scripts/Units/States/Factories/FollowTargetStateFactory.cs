namespace Underground.Units.States.Factories
{
    public class FollowTargetStateFactory
    {
        readonly FollowTargetState.Factory followTargetStateFactory;

        public FollowTargetStateFactory(FollowTargetState.Factory followTargetStateFactory)
        {
            this.followTargetStateFactory = followTargetStateFactory;
        }

        public FollowTargetState CreateState()
        {
            FollowTargetState followTargetState = followTargetStateFactory.Create();
            return followTargetState;
        }
    }
}
