namespace Underground.Units.States.Factories
{
    public class AttackTargetStateFactory
    {
        readonly AttackTargetState.Factory attackTargetStateFactory;

        public AttackTargetStateFactory(AttackTargetState.Factory attackTargetStateFactory)
        {
            this.attackTargetStateFactory = attackTargetStateFactory;
        }

        public AttackTargetState CreateState()
        {
            AttackTargetState attackTargetState = attackTargetStateFactory.Create();
            return attackTargetState;
        }
    }
}
