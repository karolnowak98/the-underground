namespace Underground.Units.States.Factories
{
    public class FollowNoiseSourceStateFactory
    {
        readonly FollowNoiseSourceState.Factory followNoiseSourceStateFactory;

        public FollowNoiseSourceStateFactory(FollowNoiseSourceState.Factory followNoiseSourceStateFactory)
        {
            this.followNoiseSourceStateFactory = followNoiseSourceStateFactory;
        }

        public FollowNoiseSourceState CreateState()
        {
            FollowNoiseSourceState followNoiseSourceState = followNoiseSourceStateFactory.Create();
            return followNoiseSourceState;
        }
    }
}
