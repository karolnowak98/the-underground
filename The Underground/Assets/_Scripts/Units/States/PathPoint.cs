using System;
using UnityEngine;

namespace Underground.Units.States
{
    [Serializable]
    public class PathPoint
    {
        public Vector3Int Position;
        public float WaitTime;
    }
}
