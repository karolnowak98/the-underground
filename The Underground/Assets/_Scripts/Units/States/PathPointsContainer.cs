using System.Collections.Generic;
using UnityEngine;

namespace Underground.Units.States
{
    public class PathPointsContainer : MonoBehaviour
    {
        public List<PathPoint> PathPoints;
    }
}
