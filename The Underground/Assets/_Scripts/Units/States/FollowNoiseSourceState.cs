using System.Collections.Generic;
using Underground.Audio;
using Underground.Maps;
using Underground.Pathfinding;
using Underground.Units.Settings;
using UnityEngine;
using Zenject;

namespace Underground.Units.States
{
    public class FollowNoiseSourceState : UnitState
    {
        private MapFacade mapFacade;
        private Transform transform;
        private Animator animator;
        private UnitSettings unitSettings;
        private AudioManager audioManager;
        private Node targetNode;
        private List<Node> path;
        private AStarPathfinding aStarPathfinding;
        private Unit unit;

        public FollowNoiseSourceState(
            MapFacade mapFacade,
            Transform transform,
            Animator animator,
            UnitSettings unitSettings,
            AudioManager audioManager,
            AStarPathfinding aStarPathfinding,
            Unit unit)
        {
            this.mapFacade = mapFacade;
            this.transform = transform;
            this.animator = animator;
            this.unitSettings = unitSettings;
            this.audioManager = audioManager;
            this.aStarPathfinding = aStarPathfinding;
            this.unit = unit;
        }

        public override void Init()
        {
            targetNode = mapFacade.GetNoiseSourceNode(transform.position);
            animator.SetBool("IsWalking", true);
            IsBusy = true;
            UpdateRotation();
            path = aStarPathfinding.FindPath(mapFacade.GetNodeFromPos(transform.position), targetNode, unitSettings.CanFly);
        }

        public override void Tick()
        {
            if (unit.Target != null)
            {
                IsBusy = false;
                return;
            }

            FollowNoiseSource();
        }

        public override void Deinit()
        {
            animator.SetBool("IsWalking", false);
            audioManager.StopPlaying();
        }

        private void FollowNoiseSource()
        {
            if (PathIsEmpty() == false)
                Move();
            else
                IsBusy = false;
        }

        private void UpdateRotation()
        {
            float yRotation = (transform.position.x < targetNode.CenterBottomPos.x) ? 0 : 180;
            transform.rotation = Quaternion.Euler(0, yRotation, 0);
        }

        private void Move()
        {
            if (Vector3.Distance(transform.position, path[0].CenterBottomPos) < 0.1f)
                path.RemoveAt(0);
            else
                transform.position = Vector2.MoveTowards(transform.position, path[0].CenterBottomPos + Vector3.up * 0.02f, unitSettings.RunSpeed * Time.deltaTime);
        }

        private bool PathIsEmpty()
        {
            if (path == null || path.Count == 0)
                return true;

            return false;
        }

        public class Factory : PlaceholderFactory<FollowNoiseSourceState>
        {
        }
    }
}
