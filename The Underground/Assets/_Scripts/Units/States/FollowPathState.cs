using System.Collections.Generic;
using Underground.Audio;
using Underground.Maps;
using Underground.Pathfinding;
using Underground.Players;
using Underground.Units.Settings;
using UnityEngine;
using Zenject;

namespace Underground.Units.States
{
    public class FollowPathState : UnitState
    {
        private List<PathPoint> pathPoints;
        private int currentPathPoint;
        private List<Node> path;
        private AStarPathfinding aStarPathfinding;
        private MapFacade mapFacade;
        private Transform transform;
        private float timeUntilNextMove;
        private Animator animator;
        private Unit unit;
        private GameObject player;
        private UnitSettings unitSettings;
        private AudioManager audioManager;

        public FollowPathState(
            AStarPathfinding aStarPathfinding,
            MapFacade mapFacade,
            Transform transform,
            PathPointsContainer pathPointsContainer,
            Animator animator,
            Unit unit,
            Player player,
            UnitSettings unitSettings,
            AudioManager audioManager)
        {
            this.aStarPathfinding = aStarPathfinding;
            this.mapFacade = mapFacade;
            this.transform = transform;
            pathPoints = pathPointsContainer.PathPoints;
            this.animator = animator;
            this.unit = unit;
            this.player = player.gameObject;
            this.unitSettings = unitSettings;
            this.audioManager = audioManager;
        }

        public override void Init()
        {
            currentPathPoint = -1;
            animator.SetBool("IsWalking", true);
        }

        public override void Tick()
        {
            FollowPath();
        }

        public override void Deinit()
        {
            animator.SetBool("IsWalking", false);
            audioManager.StopPlaying();
        }

        private void FollowPath()
        {
            if (pathPoints == null || pathPoints.Count == 0)
                return;

            if (timeUntilNextMove > 0f)
            {
                timeUntilNextMove -= Time.deltaTime;
                if(timeUntilNextMove <= 0f)
                {
                    UpdateRotation();
                    animator.SetBool("IsWalking", true);
                    audioManager.Play("Moving");
                }
                else
                    return;
            }

            if (PathIsEmpty())
            {
                animator.SetBool("IsWalking", false);
                currentPathPoint = GetNextPathPoint();
                path = aStarPathfinding.FindPath(
                    mapFacade.GetNodeFromPos(transform.position),
                    mapFacade.GetNodeFromPos(pathPoints[currentPathPoint].Position), unitSettings.CanFly);

                timeUntilNextMove = pathPoints[currentPathPoint].WaitTime;
                audioManager.StopPlaying();
                if (PathIsEmpty())
                    return;
            }

            Move();
        }

        private bool PathIsEmpty()
        {
            if (path == null || path.Count == 0)
                return true;

            return false;
        }

        private int GetNextPathPoint()
        {
            if (currentPathPoint == pathPoints.Count - 1)
                return 0;
            else
                return currentPathPoint + 1;
        }

        private void UpdateRotation()
        {
            if (PathIsEmpty())
                return;

            float yRotation = (transform.position.x < path[0].CenterBottomPos.x) ? 0 : 180;
            transform.rotation = Quaternion.Euler(0, yRotation, 0);
        }

        private void Move()
        {
            if (Vector3.Distance(transform.position, path[0].CenterBottomPos) < 0.2f)
            {
                path.RemoveAt(0);
                UpdateRotation();
            }
            else
                transform.position = Vector2.MoveTowards(transform.position, path[0].CenterBottomPos + Vector3.up * 0.02f, unitSettings.WalkSpeed * Time.deltaTime);
        }

        public class Factory : PlaceholderFactory<FollowPathState>
        {
        }
    }
}
