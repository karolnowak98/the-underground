using UnityEngine;
using System;

namespace Underground.Units.Settings
{
    [Serializable]
    [CreateAssetMenu(fileName = "UnitSettings", menuName = "Data/UnitSettings", order = 1)]
    public class UnitSettings : ScriptableObject
    {
        public float AttackDistance;
        public float AttackCooldown;
        public float WalkSpeed;
        public float RunSpeed;
        public float MaxHealth;
        public bool CanFly;
    }
}
