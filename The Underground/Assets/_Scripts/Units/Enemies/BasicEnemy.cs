using Underground.FieldOfViews;
using UnityEngine;
using Zenject;
using Underground.Units.States.Factories;
using Underground.Units.States;
using Underground.Players;
using Underground.Audio.SoundMaps;

using System;

namespace Underground.Units.Enemies
{
    public class BasicEnemy : Unit
    {
        private FollowPathStateFactory followPathStateFactory;
        private FollowTargetStateFactory followTargetStateFactory;
        private AttackTargetStateFactory attackTargetStateFactory;
        private FollowNoiseSourceStateFactory followNoiseSourceStateFactory;
        private Player player;
        private FieldOfView fieldOfView;
        private SoundMapAgent soundMapAgent;
        private bool enemyHearSomething;

        public float CooldownToNextAttack { get; set; }

        [Inject]
        public void Construct(
            Player player,
            FieldOfView fieldOfView,
            SoundMapAgent soundMapAgent,
            FollowPathStateFactory followPathStateFactory,
            FollowTargetStateFactory followTargetStateFactory,
            AttackTargetStateFactory attackTargetStateFactory,
            FollowNoiseSourceStateFactory followNoiseSourceStateFactory)
        {
            this.player = player;
            this.fieldOfView = fieldOfView;
            this.soundMapAgent = soundMapAgent;
            this.followPathStateFactory = followPathStateFactory;
            this.followTargetStateFactory = followTargetStateFactory;
            this.attackTargetStateFactory = attackTargetStateFactory;
            this.followNoiseSourceStateFactory = followNoiseSourceStateFactory;
        }

        public override void Init()
        {
            base.Init();
            fieldOfView.Init();
            enemyHearSomething = false;
        }

        public override void Tick()
        {
            base.Tick();
            fieldOfView.Tick();
            UpdateTarget();
            CheckIfEnemyHearSomething();
            CheckAndChangeCurrentState();
            UpdateCooldownToNextAttack();
        }

        private void CheckIfEnemyHearSomething()
        {
            if (soundMapAgent.NodeContainSound(gameObject.transform.position))
            {
                enemyHearSomething = true;
            }
            else
                enemyHearSomething = false;
        }

        private void UpdateTarget()
        {
            if (fieldOfView.VisibleObjects.Contains(player.gameObject))
                Target = player.gameObject;
            else
                Target = null;
        }

        private void CheckAndChangeCurrentState()
        {
            if (Target != null && player.IsInvisible == false)
            {
                if (ConditionToSetAttackTargetState())
                {
                    if (currentState.GetType() != typeof(AttackTargetState))
                        SetState(attackTargetStateFactory.CreateState());
                }
                else if (currentState.GetType() != typeof(FollowTargetState) && currentState.IsBusy == false)
                    SetState(followTargetStateFactory.CreateState());
            }
            else if (currentState.GetType() != typeof(FollowNoiseSourceState) && enemyHearSomething && currentState.IsBusy == false)
                SetState(followNoiseSourceStateFactory.CreateState());
            else if (currentState.GetType() != typeof(FollowPathState) && currentState.IsBusy == false)
                SetState(followPathStateFactory.CreateState());
        }

        private bool ConditionToSetAttackTargetState()
        {
            if (Vector3.Distance(transform.position, Target.transform.position) <= unitSettings.AttackDistance)
                return true;
            return false;
        }

        private void UpdateCooldownToNextAttack()
        {
            if (CooldownToNextAttack > 0)
                CooldownToNextAttack -= Time.deltaTime;
        }
    }
}
