using Zenject;

namespace Underground.Units.Enemies.Installers
{
    public class BasicEnemyInstaller : MonoInstaller<BasicEnemyInstaller>
    {
        public BasicEnemy BasicEnemy;

        public override void InstallBindings()
        {
            Container.BindInstance(BasicEnemy).AsSingle();
        }
    }
}
