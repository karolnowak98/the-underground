using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Underground.Units
{
    public class UnitsManager
    {
        private Transform unitsContainer;
        private List<Unit> units = new List<Unit>();

        public UnitsManager([Inject(Id = "unitsContainer")] Transform unitsContainer)
        {
            this.unitsContainer = unitsContainer;
        }

        public void Init()
        {
            for(int i = 0; i < unitsContainer.childCount; i++)
                units.Add(unitsContainer.GetChild(i).GetComponent<Unit>());

            for(int i = 0; i < units.Count; i++)
                units[i].Init();
        }

        public void Tick()
        {
            for (int i = 0; i < units.Count; i++)
                units[i].Tick();
        }

        public void FixedTick()
        {
            for (int i = 0; i < units.Count; i++)
                units[i].FixedTick();
        }

        public void Deinit()
        {
            for (int i = 0; i < units.Count; i++)
                units[i].Deinit();
        }

        public void RemoveUnit(Unit unit)
        {
            if (units.Contains(unit))
                units.Remove(unit);
        }

        public void SetActiveAnimators(bool value)
        {
            for (int i = 0; i < units.Count; i++)
                units[i].SetActiveAnimator(value);
        }
    }
}
