using Zenject;

namespace Underground.Units.Installers
{
    public class UnitInstaller : MonoInstaller<UnitInstaller>
    {
        public Unit unit;

        public override void InstallBindings()
        {
            Container.BindInstance(unit).AsSingle();
        }
    }
}
