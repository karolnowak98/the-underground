using UnityEngine;
using Zenject;
using System;
using Underground.Units.Settings;

namespace Underground.Units.Installers
{
    [Serializable]
    [CreateAssetMenu(fileName = "UnitSettingsInstaller", menuName = "Installers/UnitSettingsInstaller", order = 1)]
    public class UnitSettingsInstaller : ScriptableObjectInstaller
    {
        public UnitSettings UnitSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(UnitSettings);
        }
    }
}
