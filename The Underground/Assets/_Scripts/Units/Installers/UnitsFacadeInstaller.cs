using Zenject;

namespace Underground.Units.Installers
{
    public class UnitsFacadeInstaller : MonoInstaller<UnitsFacadeInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<UnitsFacade>().FromSubContainerResolve().ByMethod(InstallUnitsFacade).AsSingle();
        }

        private void InstallUnitsFacade(DiContainer subContainer)
        {
            subContainer.Bind<UnitsFacade>().AsSingle();
            subContainer.Bind<UnitsManager>().AsSingle();
        }
    }
}
