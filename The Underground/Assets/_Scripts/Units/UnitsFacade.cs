namespace Underground.Units
{
    public class UnitsFacade
    {
        private UnitsManager unitsManager;

        public UnitsFacade(UnitsManager unitsManager)
        {
            this.unitsManager = unitsManager;
        }

        public void Init()
        {
            unitsManager.Init();
        }

        public void Tick()
        {
            unitsManager.Tick();
        }

        public void FixedTick()
        {
            unitsManager.FixedTick();
        }

        public void Deinit()
        {
            unitsManager.Deinit();
        }

        public void RemoveUnit(Unit unit)
        {
            unitsManager.RemoveUnit(unit);
        }

        public void SetActiveAnimators(bool value)
        {
            unitsManager.SetActiveAnimators(value);
        }
    }
}
