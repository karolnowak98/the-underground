using System.Collections;
using Underground.Audio;
using Underground.FogOfWar;
using Underground.Units.Settings;
using Underground.Units.States;
using Underground.Units.States.Factories;
using UnityEngine;
using Zenject;

namespace Underground.Units
{
    public class Unit : MonoBehaviour
    {
        protected UnitState currentState;
        protected Animator animator;
        protected UnitSettings unitSettings;
        protected InvisibleInFogOfWar invisibleInFogOfWar;
        protected UnitsFacade unitsFacade;
        protected float currentHealth;
        protected bool isDead;
        protected AudioManager audioManager;
        protected AudioSource audioSource;
        
        public GameObject Target { get; set; }

        [Inject]
        private void Construct(
            FollowPathStateFactory followPathStateFactory,
            Animator animator,
            UnitSettings unitSettings,
            UnitsFacade unitsFacade,
            InvisibleInFogOfWar invisibleInFogOfWar,
            AudioManager audioManager,
            AudioSource audioSource)
        {
            currentState = followPathStateFactory.CreateState();
            this.animator = animator;
            this.unitSettings = unitSettings;
            this.unitsFacade = unitsFacade;
            this.invisibleInFogOfWar = invisibleInFogOfWar;
            this.audioManager = audioManager;
            this.audioSource = audioSource;
        }

        public virtual void Init()
        {
            if (isDead)
                return;
            currentState.Init();
            currentHealth = unitSettings.MaxHealth;
            invisibleInFogOfWar.Init();
        }

        public virtual void Tick()
        {
            if (isDead)
                return;
            currentState.Tick();
        }

        public virtual void FixedTick()
        {
            if (isDead)
                return;
        }

        public virtual void Deinit()
        {
            if (isDead)
                return;
        }

        public virtual void SetState(UnitState nextUnitState)
        {
            if (isDead)
                return;

            currentState.Deinit();
            currentState = nextUnitState;
            currentState.Init();
        }

        public void TakeDamage(float amount)
        {
            if (isDead)
                return;

            currentHealth -= amount;
            if (currentHealth <= 0)
            {
                isDead = true;
                audioSource.loop = false;
                audioManager.Play("Death");
                StartCoroutine("OnDead");
            }
        }

        private IEnumerator OnDead()
        {
            animator.SetBool("IsDead", true);
            yield return new WaitForSeconds(2f);
            unitsFacade.RemoveUnit(this);
            invisibleInFogOfWar.Deinit();
            Destroy(gameObject);
        }

        public void SetActiveAnimator(bool value)
        {
            if (animator != null)
                animator.enabled = value;
        }
    }
}
