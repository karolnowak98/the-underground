using Underground.FogOfWar.Signals;
using Zenject;

namespace Underground.FogOfWar.Installers
{
    public class FogOfWarInstaller : MonoInstaller<FogOfWarInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<FogOfWarManager>().FromSubContainerResolve().ByMethod(InstallFogOfWarManager).AsSingle();
            InstallSignals();
        }

        private void InstallFogOfWarManager(DiContainer subContainer)
        {
            subContainer.Bind<FogOfWarManager>().AsSingle();
        }

        private void InstallSignals()
        {
            Container.DeclareSignal<AddObjectToInvisibleInFogOfWarSignal>();
            Container.DeclareSignal<RemoveObjectToInvisibleInFogOfWarSignal>();
        }
    }
}
