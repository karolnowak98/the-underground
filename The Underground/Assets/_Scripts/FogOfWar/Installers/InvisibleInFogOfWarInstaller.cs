using Zenject;

namespace Underground.FogOfWar.Installers
{
    public class InvisibleInFogOfWarInstaller : MonoInstaller<InvisibleInFogOfWarInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<InvisibleInFogOfWar>().AsSingle();
        }
    }
}
