using System.Collections.Generic;
using Underground.FieldOfViews;
using Underground.Maps;
using UnityEngine;
using Zenject;

public class FogOfWarAgent : MonoBehaviour
{
    private FieldOfView fieldOfView;
    private MapFacade mapFacade;
    private List<Node> nodesToIncrementAlpha;
    private List<Node> nodesSeenThisTick;
    private Vector2 fieldOfViewStartPoint;

    [Inject]
    private void Construct(
        FieldOfView fieldOfView,
        MapFacade mapFacade)
    {
        this.fieldOfView = fieldOfView;
        this.mapFacade = mapFacade;
    }

    private void Start()
    {
        nodesToIncrementAlpha = new List<Node>();
        nodesSeenThisTick = new List<Node>();
    }

    private void FixedUpdate()
    {
        fieldOfViewStartPoint = fieldOfView.FieldOfViewStartPoint.position;

        for (int i = nodesToIncrementAlpha.Count - 1; i >= 0; i--)
            AddAlphaVisibility(nodesToIncrementAlpha[i], 16);

        nodesSeenThisTick.Clear();

        foreach (Vector2 rayVector in fieldOfView?.RaysVectors)
        {
            for (int i = 0; i < Mathf.CeilToInt(rayVector.magnitude) * 2; i++)
                ConsiderRayVector(rayVector, i / 2f);

            ConsiderRayVector(rayVector, rayVector.magnitude);
        }
    }

    private void AddAlphaVisibility(Node node, int amount)
    {
        node.CurrentAlphaVisibility += amount;
        if (node.CurrentAlphaVisibility >= node.MaxAlphaVisibility)
        {
            node.CurrentAlphaVisibility = node.MaxAlphaVisibility;
            nodesToIncrementAlpha.Remove(node);
        }
    }

    private void ConsiderRayVector(Vector2 rayVector, float rayDistance)
    {
        Vector2 position = fieldOfViewStartPoint + rayVector.normalized * rayDistance;
        Node node = mapFacade.GetNodeFromPos(position);
        if (node != null && nodesSeenThisTick.Contains(node) == false)
        {
            nodesSeenThisTick.Add(node);
            SubtractAlphaVisibility(node, 32);
        }

        if (node != null && nodesToIncrementAlpha.Contains(node) == false)
            nodesToIncrementAlpha.Add(node);
    }

    private void SubtractAlphaVisibility(Node node, int amount)
    {
        node.CurrentAlphaVisibility -= amount;
        if (node.CurrentAlphaVisibility < 0)
            node.CurrentAlphaVisibility = 0;
    }
}
