using Underground.FogOfWar.Signals;
using UnityEngine;
using Zenject;

namespace Underground.FogOfWar
{
    public class InvisibleInFogOfWar
    {
        private FogOfWarManager fogOfWarManager;
        private SignalBus signalBus;
        private SpriteRenderer spriteRenderer;
        private AudioSource audioSource;

        [Inject]
        private void Construct(
            FogOfWarManager fogOfWarManager,
            SpriteRenderer spriteRenderer,
            SignalBus signalBus,
            AudioSource audioSource)
        {
            this.fogOfWarManager = fogOfWarManager;
            this.spriteRenderer = spriteRenderer;
            this.signalBus = signalBus;
            this.audioSource = audioSource;
        }

        public void Init()
        {
            AddToInsivibleObjects();
        }

        public void Deinit()
        {
            RemoveToInsivibleObjects();
        }

        private void AddToInsivibleObjects()
        {
            signalBus.Fire(new AddObjectToInvisibleInFogOfWarSignal() { SpriteRenderer = spriteRenderer, AudioSource = audioSource });
        }

        private void RemoveToInsivibleObjects()
        {
            signalBus.Fire(new RemoveObjectToInvisibleInFogOfWarSignal() { SpriteRenderer = spriteRenderer, AudioSource = audioSource });
        }
    }
}
