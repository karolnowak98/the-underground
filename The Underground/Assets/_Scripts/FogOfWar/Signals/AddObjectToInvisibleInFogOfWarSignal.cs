using UnityEngine;

namespace Underground.FogOfWar.Signals
{
    public class AddObjectToInvisibleInFogOfWarSignal
    {
        public SpriteRenderer SpriteRenderer;
        public AudioSource AudioSource;
    }
}
