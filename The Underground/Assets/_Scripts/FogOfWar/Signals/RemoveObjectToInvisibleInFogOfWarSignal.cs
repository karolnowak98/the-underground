using UnityEngine;

namespace Underground.FogOfWar.Signals
{
    public class RemoveObjectToInvisibleInFogOfWarSignal
    {
        public SpriteRenderer SpriteRenderer;
        public AudioSource AudioSource;
    }
}
