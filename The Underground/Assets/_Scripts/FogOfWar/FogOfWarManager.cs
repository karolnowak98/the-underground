using System.Collections.Generic;
using Underground.FogOfWar.Signals;
using Underground.Maps;
using UnityEngine;
using Zenject;

public class FogOfWarManager
{
    private MapFacade mapFacade;
    public SpriteRenderer fogOfWarSpriteRenderer;
    private SignalBus signalBus;
    private List<SpriteRenderer> followedSpriteRenderers;
    private List<AudioSource> followedAudioSources;

    [Inject]
    private void Construct(MapFacade mapFacade,
        [Inject (Id = "FogOfWarSprite")] SpriteRenderer fogOfWarSpriteRenderer,
        SignalBus signalBus)
    {
        this.mapFacade = mapFacade;
        this.fogOfWarSpriteRenderer = fogOfWarSpriteRenderer;
        this.signalBus = signalBus;
    }

    public void Init()
    {
        //fogOfWarSpriteRenderer.transform.localScale = new Vector3(mapFacade.MapSize().x * 2f, mapFacade.MapSize().y * 2f, 1);
        fogOfWarSpriteRenderer.transform.localScale = new Vector3(100,100, 1);
        fogOfWarSpriteRenderer.transform.localPosition = new Vector3(mapFacade.MapSize().x / 2f, mapFacade.MapSize().y / 2f, 0);
        followedSpriteRenderers = new List<SpriteRenderer>();
        followedAudioSources = new List<AudioSource>();
        signalBus.Subscribe<AddObjectToInvisibleInFogOfWarSignal>(AddObject);
        signalBus.Subscribe<RemoveObjectToInvisibleInFogOfWarSignal>(RemoveObject);
    }

    public void Tick()
    {
        UpdateFogOfWarSprite();
        UpdateFogOfWarObjects();
    }

    private void UpdateFogOfWarSprite()
    {
        Texture2D texture2D = new Texture2D(mapFacade.MapSize().x, mapFacade.MapSize().y);
        texture2D.wrapMode = TextureWrapMode.Clamp;
        texture2D.filterMode = FilterMode.Bilinear;
        for (int y = 0; y < mapFacade.MapSize().y; y++)
        {
            for (int x = 0; x < mapFacade.MapSize().x; x++)
            {
                byte value = (byte)mapFacade.GetNodeFromGrid(x, y).CurrentAlphaVisibility;
                Color col = new Color32(0, 0, 0, value);
                texture2D.SetPixel(x, y, col);
            }
        }

        texture2D.Apply();
        Sprite spr = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));
        fogOfWarSpriteRenderer.sprite = spr;
    }

    private void UpdateFogOfWarObjects()
    {
        for(int i = 0; i < followedSpriteRenderers.Count; i++)
        {
            if (mapFacade.GetNodeFromPos(followedSpriteRenderers[i].gameObject.transform.position).CurrentAlphaVisibility < 127)
            {
                followedSpriteRenderers[i].color = new Color(1, 1, 1, 1);
                followedAudioSources[i].mute = false;
            }
            else
            {
                followedSpriteRenderers[i].color = new Color(1, 1, 1, 0);
                followedAudioSources[i].mute = true;
            }
        }
    }

    private void AddObject(AddObjectToInvisibleInFogOfWarSignal signal)
    {
        followedSpriteRenderers.Add(signal.SpriteRenderer);
        followedAudioSources.Add(signal.AudioSource);
    }

    private void RemoveObject(RemoveObjectToInvisibleInFogOfWarSignal signal)
    {
        followedSpriteRenderers.Remove(signal.SpriteRenderer);
        followedAudioSources.Remove(signal.AudioSource);
    }
}
