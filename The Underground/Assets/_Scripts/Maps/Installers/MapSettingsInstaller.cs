using UnityEngine;
using Zenject;
using System;
using Underground.Maps.Settings;

namespace Underground.Maps.Installers
{
    [Serializable]
    [CreateAssetMenu(fileName = "MapSettingsInstaller", menuName = "Installers/MapSettingsInstaller", order = 1)]
    public class MapSettingsInstaller : ScriptableObjectInstaller
    {
        public MapSettings MapSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(MapSettings);
        }
    }
}
