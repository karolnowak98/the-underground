using UnityEngine;
using Zenject;
using Underground.Maps.Settings;

namespace Underground.Maps.Installers
{
    public class MapInstaller : MonoInstaller<MapInstaller>
    {
        [SerializeField] private MapSettingsInstaller mapSettingsInstaller;

        public override void InstallBindings()
        {
            Container.Bind<MapFacade>().FromSubContainerResolve().ByMethod(InstallMapFacade).AsSingle();
        }

        private void InstallMapFacade(DiContainer subContainer)
        {
            subContainer.Bind<MapFacade>().AsSingle();

            subContainer.Bind<Map>().AsSingle();
            subContainer.Bind<MapSettings>().FromInstance(mapSettingsInstaller.MapSettings);
        }
    }
}
