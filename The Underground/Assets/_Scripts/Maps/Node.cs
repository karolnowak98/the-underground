using UnityEngine;

namespace Underground.Maps
{
    public class Node
    {
        public bool Walkable;
        public bool Flyable;
        public int CurrentAlphaVisibility;
        public int MaxAlphaVisibility;
        public int SoundLevel;

        public Vector3 Pos;
        public Vector3 CenterPos;
        public Vector3 CenterBottomPos;

        public int XId, YId;

        public Node Parent;
        public int GCost;
        public int HCost;
        public int FCost { get => GCost + HCost; }

        public Node(bool walkable, bool flyable, int maxAlphaVisibility, int soundLevel, Vector3 pos, int xId, int yId)
        {
            CurrentAlphaVisibility = 255;
            Walkable = walkable;
            Flyable = flyable;
            MaxAlphaVisibility = maxAlphaVisibility;
            SoundLevel = soundLevel;
            Pos = pos;
            CenterPos = new Vector3(pos.x + 0.5f, pos.y + 0.5f, pos.z);
            CenterBottomPos = new Vector3(pos.x + 0.5f, pos.y, pos.z);
            XId = xId;
            YId = yId;
        }
    }
}
