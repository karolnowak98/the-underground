using UnityEngine;
using System;

namespace Underground.Maps.Settings
{
    [Serializable]
    [CreateAssetMenu(fileName = "MapSettings", menuName = "Data/MapSettings", order = 1)]
    public class MapSettings : ScriptableObject
    {
        public Vector2Int MapSize;
        public bool[] Walkable;
        public int[] MaxAlphaVisibility;
        public bool[] Flyable;
        public int[] SoundLevel;
        public bool LevelCompleted;
    }
}
