using System.Collections.Generic;
using UnityEngine;

namespace Underground.Maps
{
    public class MapFacade
    {
        private Map map;

        public MapFacade(Map map)
        {
            this.map = map;
        }

        public void Init()
        {
            map.Init();
        }

        public Vector2Int MapSize()
        {
            return map.MapSize;
        }

        public Node GetNodeFromGrid(int xId, int yId)
        {
            return map.Grid[xId, yId];
        }

        public Node GetNodeFromPos(Vector3 position)
        {
            return map.GetNodeFromPos(position);
        }

        public List<Node> GetNeighbours(Node node)
        {
            return map.GetNeighbours(node);
        }

        public List<Node> GetNeighbours4(Node node)
        {
            return map.GetNeighbours4(node);
        }

        public Node GetRandomWalkableNode()
        {
            return map.GetRandomWalkableNode();
        }

        public Node GetNoiseSourceNode(Vector3 position)
        {
            return map.GetNoiseSourceNode(position);
        }
    }
}
