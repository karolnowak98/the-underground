using Underground.Maps.Settings;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Underground.Maps
{
    public class MapEditor : MonoBehaviour
    {
        [SerializeField] private bool showGizmos;
        [SerializeField] private bool showCurrentVisibility;
        [SerializeField] private MapSettings mapSettings;
        [SerializeField] private InputAction inputAction;
        private Vector3 screenMousePosition;
        private Vector3 worldMousePosition;
        private MapFacade mapFacade;
        [SerializeField] private bool changeWalkable;
        [SerializeField] private bool changeVisibility;
        [SerializeField] private bool ChangeFlyable;
        [SerializeField] private bool changeSoundLevel;

        [Inject]
        private void Construct(MapFacade mapFacade)
        {
            this.mapFacade = mapFacade;
        }

        public void Update()
        {
            CalculateWorldMousePosition();

            if (worldMousePosition.x >= 0 && worldMousePosition.x < mapSettings.MapSize.x && worldMousePosition.y >= 0 && worldMousePosition.y < mapSettings.MapSize.y)
            {
                if (changeWalkable)
                {
                    if (Mouse.current.leftButton.isPressed)
                        mapSettings.Walkable[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = true;
                    if (Mouse.current.rightButton.isPressed)
                        mapSettings.Walkable[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = false;
                }
                if (ChangeFlyable)
                {
                    if (Mouse.current.leftButton.isPressed)
                        mapSettings.Flyable[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = true;
                    if (Mouse.current.rightButton.isPressed)
                        mapSettings.Flyable[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = false;
                }
                if (changeVisibility)
                {
                    if (Mouse.current.leftButton.isPressed)
                        mapSettings.MaxAlphaVisibility[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = 127;
                    if (Mouse.current.rightButton.isPressed)
                        mapSettings.MaxAlphaVisibility[(int)worldMousePosition.x + (int)worldMousePosition.y * mapSettings.MapSize.x] = 255;
                }
            }
        }

        private void CalculateWorldMousePosition()
        {
            worldMousePosition = GetWorldMousePosition(screenMousePosition);
            worldMousePosition = new Vector3(Mathf.FloorToInt(worldMousePosition.x), Mathf.FloorToInt(worldMousePosition.y), 0);
        }

        public void ReadScreenMousePosition(InputAction.CallbackContext context)
        {
            screenMousePosition = context.ReadValue<Vector2>();
        }

        private Vector3 GetWorldMousePosition(Vector3 screenMousePosition)
        {
            screenMousePosition.z = 10;
            screenMousePosition = Camera.main.ScreenToWorldPoint(screenMousePosition);
            screenMousePosition.z = 0;
            return screenMousePosition;
        }

        private void OnDrawGizmos()
        {
            if (!showGizmos)
                return;

            for (int y = 0; y < mapSettings.MapSize.y; y++)
            {
                for (int x = 0; x < mapSettings.MapSize.x; x++)
                {
                    if (changeWalkable)
                        Gizmos.color = (mapSettings.Walkable[x + y * mapSettings.MapSize.x]) ? Color.green : Color.red;
                    else if (ChangeFlyable)
                        Gizmos.color = (mapSettings.Flyable[x + y * mapSettings.MapSize.x]) ? Color.green : Color.red;
                    else if (changeVisibility)
                    {
                        int value = mapSettings.MaxAlphaVisibility[x + y * mapSettings.MapSize.x];
                        Gizmos.color = new Color32((byte)value, (byte)value, (byte)value, 255);
                    }
                    else if (showCurrentVisibility)
                    {
                        int value = mapFacade.GetNodeFromGrid(x, y).CurrentAlphaVisibility;
                        Gizmos.color = new Color32(0, (byte)value, 0, 255);
                    }
                    else if(changeSoundLevel)
                    {
                        int value = mapFacade.GetNodeFromGrid(x, y).SoundLevel;
                        if (value == 5)Gizmos.color = Color.red;
                        else if(value == 4)Gizmos.color = Color.yellow;
                        else if(value == 3)Gizmos.color = Color.green;
                        else if(value == 2)Gizmos.color = Color.cyan;
                        else if(value == 1) Gizmos.color = Color.blue;
                        else if(value == 0) Gizmos.color = Color.white;
                        else 
                            Gizmos.color = Color.black;

                    }

                    Gizmos.DrawCube(new Vector3(x + 0.5f, y + 0.5f), Vector3.one * 0.3f);
                }
            }

            Gizmos.color = Color.gray;
            Gizmos.DrawCube(worldMousePosition + new Vector3(0.5f, 0.5f), Vector3.one * 0.4f);
        }
    }
}
