using System.Collections.Generic;
using Underground.Maps.Settings;
using UnityEngine;

namespace Underground.Maps
{
    public class Map
    {
        private MapSettings mapSettings;

        public Vector2Int MapSize { get; set; }
        public Node[,] Grid { get; private set; }

        private Map(MapSettings mapSettings)
        {
            this.mapSettings = mapSettings;
        }

        public void Init()
        {
            MapSize = mapSettings.MapSize;
            GenerateGrid();
        }

        private void GenerateGrid()
        {
            Grid = new Node[MapSize.x, MapSize.y];

            for (int y = 0; y < MapSize.y; y++)
            {
                for (int x = 0; x < MapSize.x; x++)
                {
                    bool walkable = mapSettings.Walkable[x + y * mapSettings.MapSize.x];
                    bool flyable = mapSettings.Flyable[x + y * mapSettings.MapSize.x];
                    int maxAlphaVisibility = mapSettings.MaxAlphaVisibility[x + y * mapSettings.MapSize.x];
                    int soundLevel = mapSettings.SoundLevel[x + y * mapSettings.MapSize.x];
                    Vector3 pos = new Vector3(x, y, 0);

                    Grid[x, y] = new Node(walkable, flyable, maxAlphaVisibility,soundLevel, pos, x, y);
                }
            }
        }

        public Node GetNoiseSourceNode(Vector3 position)
        {
            Node nodeWithHighesNoise = GetNodeFromPos(position);
            bool higherNoiseNodeNotFound = false;
            for (int i = 0; i < 10 && !higherNoiseNodeNotFound; i++)
            {
                List<Node> neighbors = GetNeighbours(nodeWithHighesNoise);
                higherNoiseNodeNotFound = true;
                foreach (Node node in neighbors)
                {
                    if(node.SoundLevel > nodeWithHighesNoise.SoundLevel)
                    {
                        nodeWithHighesNoise = node;
                        higherNoiseNodeNotFound = false;
                        break;
                    }
                }
            }
            return nodeWithHighesNoise;
        }

        public Node GetNodeFromPos(Vector3 position)
        {
            if (position.x < 0 || position.x >= MapSize.x || position.y < 0 || position.y >= MapSize.y)
                return null;

            return Grid[Mathf.FloorToInt(position.x), Mathf.FloorToInt(position.y)];
        }

        public List<Node> GetNeighbours(Node node)
        {
            List<Node> neighbours = new List<Node>();

            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    int checkX = node.XId + x;
                    int checkY = node.YId + y;

                    neighbours.Add(Grid[checkX, checkY]);
                }
            }

            return neighbours;
        }

        public List<Node> GetNeighbours4(Node node)
        {
            List<Node> neighbours4 = new List<Node>();
            if (node.YId + 1 < MapSize.y)
                neighbours4.Add(Grid[node.XId, node.YId + 1]);
            if (node.XId + 1 < MapSize.x)
                neighbours4.Add(Grid[node.XId + 1, node.YId]);
            if (node.YId - 1 >= 0)
                neighbours4.Add(Grid[node.XId, node.YId - 1]);
            if (node.XId - 1 >= 0)
                neighbours4.Add(Grid[node.XId - 1, node.YId]);

            return neighbours4;
        }

        public Node GetRandomWalkableNode()
        {
            Node randomWalkableNode = null;

            while(randomWalkableNode == null)
            {
                int randomNodeXId = Random.Range(0, Grid.GetLength(0));
                int randomNodeYId = Random.Range(0, Grid.GetLength(1));

                if (Grid[randomNodeXId, randomNodeYId].Walkable == true)
                    randomWalkableNode = Grid[randomNodeXId, randomNodeYId];
            }

            return randomWalkableNode;
        }
    }
}
