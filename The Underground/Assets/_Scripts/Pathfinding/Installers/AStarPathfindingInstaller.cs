using Zenject;

namespace Underground.Pathfinding.Installers
{
    public class AStarPathfindingInstaller : MonoInstaller<AStarPathfindingInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<AStarPathfinding>().FromSubContainerResolve().ByMethod(InstallAStarPathfinding).AsSingle().NonLazy();
        }

        private void InstallAStarPathfinding(DiContainer subContainer)
        {
            subContainer.Bind<AStarPathfinding>().AsSingle().NonLazy();
        }
    }
}
